import PropTypes from 'prop-types';
import ThemeProvider from './theme-provider';
import ResponsiveProvider from './responsive-provider';
import { ToastContainer, Slide } from 'react-toastify';

const RootProvider = ({ children }) => {
  return (
    <ThemeProvider>
      <ResponsiveProvider />
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={true}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        transition={Slide}
      />
      {children}
    </ThemeProvider>
  );
};

RootProvider.propTypes = {
  children: PropTypes.node,
};
export default RootProvider;
