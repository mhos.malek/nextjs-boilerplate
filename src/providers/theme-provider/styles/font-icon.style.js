import { createGlobalStyle } from 'styled-components';

const GlobalProductFactoryIcons = createGlobalStyle`

@font-face {
    font-family: 'PF-icons';
    src:  url('/assets/font/pf-font-icon/pf-icons.eot?7cxsk5');
    src:  url('/assets/font/pf-font-icon/pf-icons.eot?7cxsk5#iefix') format('embedded-opentype'),
      url('/assets/font/pf-font-icon/pf-icons.ttf?7cxsk5') format('truetype'),
      url('/assets/font/pf-font-icon/pf-icons.woff?7cxsk5') format('woff'),
      url('/assets/font/pf-font-icon/pf-icons.svg?7cxsk5#pf-icons') format('svg');
    font-weight: normal;
    font-style: normal;
    font-display: block;
  }
  
  [class^="icon-"], [class*=" icon-"] {
    /* use !important to prevent issues with browser extensions that change fonts */
    font-family: 'PF-icons' !important;
    speak: none;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    line-height: 1;
  
    /* Better Font Rendering =========== */
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
  

  [class^="icon-"], [class*=" icon-"] {
    /* use !important to prevent issues with browser extensions that change fonts */
    font-family: 'PF-icons' !important;
    speak: none;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    line-height: 1;
  
    /* Better Font Rendering =========== */
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
  
 
.icon-Back:before {
  content: "\\e900";
}
.icon-Delete:before {
  content: "\\e901";
}
.icon-Email:before {
  content: "\\e902";
}
.icon-Hidden-pass:before {
  content: "\\e903";
}
.icon-Lock:before {
  content: "\\e904";
}
.icon-Password:before {
  content: "\\e905";
}
.icon-Phone:before {
  content: "\\e906";
}
.icon-Send-Messages:before {
  content: "\\e907";
}
.icon-Show-pass:before {
  content: "\\e908";
}
.icon-Support:before {
  content: "\\e909";
}
.icon-User-add:before {
  content: "\\e90a";
}
.icon-User-Lock:before {
  content: "\\e90b";
}
.icon-User-Profile:before {
  content: "\\e90c";
}
.icon-Arrow-Down:before {
  content: "\\e90d";
}
.icon-Arrow-up:before {
  content: "\\e90e";
}
.icon-Back1:before {
  content: "\\e90f";
}
.icon-Backward:before {
  content: "\\e910";
}
.icon-Bookmark:before {
  content: "\\e911";
}
.icon-Burger:before {
  content: "\\e912";
}
.icon-captioning:before {
  content: "\\e913";
}
.icon-Chat-bubble:before {
  content: "\\e914";
}
.icon-Chat:before {
  content: "\\e915";
}
.icon-Close:before {
  content: "\\e916";
}
.icon-Documents1:before {
  content: "\\e917";
}
.icon-Done:before {
  content: "\\e918";
}
.icon-Emails:before {
  content: "\\e919";
}
.icon-Flag:before {
  content: "\\e91a";
}
.icon-Forward:before {
  content: "\\e91b";
}
.icon-Google-play:before {
  content: "\\e91c";
}
.icon-Lock1:before {
  content: "\\e91d";
}
.icon-Menu:before {
  content: "\\e91e";
}
.icon-Note:before {
  content: "\\e91f";
}
.icon-Notifications:before {
  content: "\\e920";
}
.icon-Pause-1:before {
  content: "\\e921";
}
.icon-Pause:before {
  content: "\\e922";
}
.icon-Play-2:before {
  content: "\\e923";
}
.icon-Play-Next:before {
  content: "\\e924";
}
.icon-Record:before {
  content: "\\e925";
}
.icon-Refresh:before {
  content: "\\e926";
}
.icon-Screenshots-2:before {
  content: "\\e927";
}
.icon-Screenshots:before {
  content: "\\e928";
}
.icon-Settings:before {
  content: "\\e929";
}
.icon-Share:before {
  content: "\\e92a";
}
.icon-Silent:before {
  content: "\\e92b";
}
.icon-Telegram:before {
  content: "\\e92c";
}
.icon-Thumbs-down:before {
  content: "\\e92d";
}
.icon-Thumbs-up:before {
  content: "\\e92e";
}
.icon-Timer:before {
  content: "\\e92f";
}
.icon-Twitter:before {
  content: "\\e930";
}
.icon-Volume-Full:before {
  content: "\\e931";
}
.icon-WiFi-Off:before {
  content: "\\e932";
}
.icon-Arrow-Up:before {
  content: "\\e933";
}
.icon-Award:before {
  content: "\\e934";
}
.icon-Calendar:before {
  content: "\\e935";
}
.icon-Certificate:before {
  content: "\\e936";
}
.icon-Documents:before {
  content: "\\e937";
}
.icon-Extra-Icons:before {
  content: "\\e938";
}
.icon-Eye:before {
  content: "\\e939";
}
.icon-Filled-Star:before {
  content: "\\e93a";
}
.icon-Grid-Layout:before {
  content: "\\e93b";
}
.icon-reply:before {
  content: "\\e93c";
}
.icon-Save:before {
  content: "\\e93d";
}
.icon-Schedule:before {
  content: "\\e93e";
}
.icon-Search:before {
  content: "\\e93f";
}
.icon-Shield:before {
  content: "\\e940";
}
.icon-Star-filled:before {
  content: "\\e941";
}
.icon-Star:before {
  content: "\\e942";
}
.icon-start:before {
  content: "\\e943";
}
.icon-Time:before {
  content: "\\e944";
}
  
`;

export default GlobalProductFactoryIcons;
