import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
html, body {
    margin: 0;
    padding: 0;
    font-family: Dana;
    direction: ${({ theme }) => theme.direction.rtl};
    color: ${({ theme }) => theme.colors.verylightpink};
    scroll-behavior: smooth;
    ::-webkit-scrollbar {
      width: 4px;
    }
  
    /* Track */
    ::-webkit-scrollbar-track {
      background: ${({ theme }) => theme.colors.darktwo};
    }
  
    /* Handle */
    ::-webkit-scrollbar-thumb {
      background: ${({ theme }) => theme.colors.gunmetaltwo};
    }
  
    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
      background: ${({ theme }) => theme.colors.gunmetaltwo};
    }
    &:placholder {
      color: ${({ theme }) => theme.colors.browngrey};
    }
}

p {
  margin: 0;
}
a {
  color: inherit;
  text-decoration: inherit;
  transition: all 0.3s;
  &:hover {
    color: ${({ theme }) => theme.defaultColors.primary}
  }
}
text {
  font-family: Dana!important;
}
* {
    box-sizing: border-box;
  }
`;

export default GlobalStyle;
