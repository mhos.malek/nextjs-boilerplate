import {
  mediaQueriesMaxWidth,
  mediaQueriesMinWidth,
} from '../../responsive-provider/utils';

const productFactoryTheme = () => {
  const colors = {
    verylightpink: '#eaeaea',
    dark: '#0e0e10',
    orangeyyellow: '#ffb821',
    gunmetal: '#494955',
    slategrey: '#68687a',
    darktwo: '#1e1e24',
    bluegrey: '#7f7f96',
    darknavyblue: '#000e1e',
    skyblue: '#66aefd',
    darkthree: '#121215',
    darkfour: '#131315',
    deepskyblue: '#027aff',
    gunmetaltwo: '#474758',
    blueygrey: '#9090a9',
    black70: 'rgba(0, 0, 0, 0.7)',
    white: '#ffffff',
    black90: 'rgba(0, 0, 0, 0.9)',
    black80: 'rgba(0, 0, 0, 0.8)',
    grapefruit: '#ff6060',
    blacktwo90: 'rgba(0, 0, 0, 0.9)',
    greyishbrown: '#484848',
    browngrey: '#9c9c9c',
    deepskybluetwo: '#0f65fb',
    darkfive: '#1b1b1f',
    greyishbrowntwo: '#4c4c4c',
    transparent: 'transparent',
    dangersecondary: '#d05354',
    disableGray: '#565656',
  };

  const defaultColors = {
    primary: colors.deepskyblue,
    secondary: colors.skyblue,
    danger: colors.grapefruit,
    gray: colors.browngrey,
    lightGray: colors.veryLightPink,
    white: colors.white,
    dark: colors.dark,
  };
  const dimensions = {
    xs: '2px',
    sm: '4px',
    md: '8px',
    lg: '16px',
    xl: '24px',
    xxl: '32px',
    xxxl: '48px',
    xxxxl: '52px',
    xxxxxxxxl: '72px',
    xxxxxl: '80px',
    xxxxxxl: '104px',
    full: '100%',
    fullViewPortWidth: '100vw',
    fullViewPortHeight: '100vh',
  };

  const typography = {
    defaultFont: {
      name: 'Dana',
      regular: {
        20: `
        font-size: 20px;
        font-weight: normal;
        line-height: 2;

       `,
        16: `
        font-size: 16px;
        font-weight: normal;
        line-height: 2;

       `,
        14: `
        font-size: 14px;
        font-weight: normal;
        line-height: 2;

       `,
        13: `
        font-size: 13px;
        font-weight: normal;
        line-height: 2;

  `,
        12: `
  font-size: 12px;
  font-weight: normal;
  line-height: 2;

`,
      },
      normal: {
        24: `
        font-size: 24px;
        font-weight: 500;
        line-height: 2;

       `,
        20: `
       font-size: 20px;
       font-weight: 500;
       line-height: 2;
      `,
        18: `
  font-size: 18px;
  font-weight: 500;
  line-height: 2;

  `,
        16: `
  font-size: 16px;
  font-weight: 500;
  line-height: 2;

  `,
      },
      bold: {
        34: `
        font-size: 34px;
        font-weight: bold;
        line-height: 2;
       `,
        24: `
        font-size: 24px;
        font-weight: bold;
        line-height: 2;
       `,
        22: `
       font-size: 22px;
       font-weight: bold;
       line-height: 2;
      `,
        20: `
        font-size: 20px;
        font-weight: bold;
        line-height: 2;
       `,
        16: `
        font-size: 16px;
        font-weight: bold;
        line-height: 2;
       `,
        14: `
  font-size: 14px;
  font-weight: bold;
  line-height: 2;
  `,
      },
    },
    englishFont: {
      name: 'Tahoma',
      regular: {
        16: `
        font-family: Tahoma;
        font-size: 16px;
        font-weight: normal;
        line-height: 2;

       `,
        14: `
        font-family: Tahoma;
        font-size: 14px;
        font-weight: normal;
        line-height: 2;

       `,
        13: `
        font-family: Tahoma;
        font-size: 13px;
        font-weight: normal;
        line-height: 2;

  `,
        12: `
        font-family: Tahoma;
  font-size: 12px;
  font-weight: normal;
  line-height: 2;

`,
      },
      normal: {
        40: `
        font-family: Tahoma;
        font-size: 40px;
        font-weight: 500;

       `,

        24: `
        font-family: Tahoma;
        font-size: 24px;
        font-weight: 500;
        line-height: 2;

       `,
        18: `
        font-family: Tahoma;
  font-size: 18px;
  font-weight: 500;
  line-height: 2;

  `,
        16: `
        font-family: Tahoma;
  font-size: 16px;
  font-weight: 500;
  line-height: 2;

  `,
      },
      bold: {
        16: `
        font-family: Tahoma;
        font-size: 16px;
        font-weight: bold;
        line-height: 2;
       `,
        14: `
        font-family: Tahoma;
  font-size: 14px;
  font-weight: bold;
  line-height: 2;
  `,
      },
    },
  };

  const direction = {
    rtl: 'rtl',
    ltr: 'ltr',
  };

  return {
    colors,
    dimensions,
    typography,
    defaultColors,
    direction,
    mediaQueriesMaxWidth,
    mediaQueriesMinWidth,
  };
};

export default productFactoryTheme;
