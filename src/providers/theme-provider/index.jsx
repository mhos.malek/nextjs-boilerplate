import React from 'react';
import PropTypes from 'prop-types';
import { ThemeProvider as StyleComponentThemeProvider } from 'styled-components';
import productFactoryTheme from './theme';
import GlobalStyle from './styles/general-style';
import GlobalProductFactoryIcons from './styles/font-icon.style';
import ReplaceCircleProgressLibStyles from '../../ui/base-components/circle-progress/style';

const ThemeProvider = ({ children }) => {
  return (
    <StyleComponentThemeProvider theme={productFactoryTheme}>
      <GlobalStyle />
      <GlobalProductFactoryIcons />
      <ReplaceCircleProgressLibStyles />

      {children}
    </StyleComponentThemeProvider>
  );
};

ThemeProvider.propTypes = {
  children: PropTypes.node,
};

export default ThemeProvider;
