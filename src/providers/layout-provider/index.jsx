import PropTypes from 'prop-types';
import AuthLayout from '../../ui/layouts/auth-layout';

const LayoutProvider = ({ children }) => {
  return <AuthLayout> {children} </AuthLayout>;
};

LayoutProvider.propTypes = {
  children: PropTypes.node,
};
export default LayoutProvider;
