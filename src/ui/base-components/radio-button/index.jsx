import PropTypes from 'prop-types';
import StyledRadioButton from './style';

const RadioButton = ({
  label,
  name,
  onChange,
  checked,
  customParentStyle,
  value,
}) => {
  return (
    <StyledRadioButton
      onClick={() => onChange && onChange(value)}
      style={{ ...customParentStyle }}
    >
      <div>{label}</div>
      <input
        type="radio"
        checked={checked}
        name={name}
        className="radioButton"
      />
      <span className="checkmark"></span>
    </StyledRadioButton>
  );
};

RadioButton.propTypes = {
  label: PropTypes.node,
  name: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.string,
  checked: PropTypes.bool,
  customParentStyle: PropTypes.string,
};

export default RadioButton;
