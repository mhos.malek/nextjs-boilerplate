import styled from 'styled-components';

const StyledRadioButton = styled.div`
  display: block;
  position: relative;
  padding-right: 35px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;

  input.radioButton {
    position: absolute;
    opacity: 0;
    cursor: pointer;
  }

  .checkmark {
    position: absolute;
    top: 0;
    right: 0;
    height: 20px;
    width: 20px;
    background-color: transparent;
    border: 2px solid ${({ theme }) => theme.defaultColors.gray};
    border-radius: 50%;
  }

  &:hover input.radioButton ~ .checkmark {
    background-color: transparent;
  }

  /* When the radio button is checked, add a blue background */
  input.radioButton:checked ~ .checkmark {
    background-color: transparent;
    border: 2px solid ${({ theme }) => theme.defaultColors.primary};
  }

  .checkmark:after {
    content: '';
    position: absolute;
    display: none;
  }

  /* Show the indicator (dot/circle) when checked */
  input.radioButton:checked ~ .checkmark:after {
    display: block;
  }

  /* Style the indicator (dot/circle) */
  .checkmark:after {
    top: 3px;
    left: 3px;
    width: 10px;
    height: 10px;
    border-radius: 50%;
    background: ${({ theme }) => theme.defaultColors.primary};
  }
`;

export default StyledRadioButton;
