import styled from 'styled-components';

const StyledProgressWrapper = styled.div`
  width: 100%;
  height: 4px;
  border-radius: 2px;
  direction: ${({ direction }) => direction};
  min-width: 250px;
  background-color: ${({ placeholderBackgroundColor }) =>
    placeholderBackgroundColor && placeholderBackgroundColor};
`;

const StyledProgress = styled.div`
  width: ${({ percent }) => percent || 0}%;
  height: 4px;
  border-radius: 2px;
  background-color: ${({ theme, color }) =>
    theme.colors[color] || color || theme.defaultColors.primary};
`;
export { StyledProgressWrapper, StyledProgress };
