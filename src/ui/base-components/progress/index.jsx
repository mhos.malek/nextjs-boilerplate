import PropTypes from 'prop-types';
import { StyledProgressWrapper, StyledProgress } from './style';
const ProgressBar = ({
  percent,
  color,
  direction,
  placeholderBackgroundColor,
}) => {
  return (
    <StyledProgressWrapper
      placeholderBackgroundColor={placeholderBackgroundColor}
    >
      <StyledProgress
        direction={direction}
        color={color}
        percent={percent}
      />
    </StyledProgressWrapper>
  );
};
ProgressBar.defaultProps = {
  direction: 'rtl',
};

ProgressBar.propTypes = {
  percent: PropTypes.number,
  color: PropTypes.string,
  direction: PropTypes.string,
  placeholderBackgroundColor: PropTypes.string,
};
export default ProgressBar;
