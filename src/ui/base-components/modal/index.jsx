import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import OverRideStyles, {
  StyledModal,
  StyledContainer,
  StyledModalHeader,
  StyledTitleWrapper,
  StyledColseButtonWrapper,
  StyledModalFooter,
  StyledModalContent,
  StyledCloseIcon,
} from './style';
import Icon from '../icon';
import Typography from '../typography';

const Modal = ({
  children,
  open,
  afterOpen,
  title,
  bgColor,
  footer,
  onClose,
}) => {
  const [modalIsOpen, setIsOpen] = useState(open);

  function afterOpenModal() {
    afterOpen && afterOpen();
  }

  function closeModal() {
    setIsOpen(false);
    onClose && onClose();
  }
  useEffect(() => {
    setIsOpen(open);
  }, [open]);

  return (
    <>
      <OverRideStyles />
      <StyledModal
        isOpen={modalIsOpen}
        onAfterOpen={afterOpenModal}
        onRequestClose={closeModal}
        contentLabel="Modal"
        className="custom-pf-modal"
        style={{
          overlay: {
            backgroundColor: 'rgba(0, 0, 0, 0.4)',
          },
        }}
      >
        <StyledContainer bgColor={bgColor}>
          <StyledModalHeader bgColor={bgColor}>
            <StyledTitleWrapper>
              <Typography
                color={
                  bgColor === 'verylightpink' ? 'dark' : '#e1e1e2'
                }
                size="16"
                type="normal"
              >
                {title}
              </Typography>
            </StyledTitleWrapper>
            <StyledColseButtonWrapper onClick={() => closeModal()}>
              <StyledCloseIcon>
                <Icon
                  name="Delete"
                  color={
                    bgColor === 'verylightpink' ? 'dark' : '#e1e1e2'
                  }
                  size="10"
                />
              </StyledCloseIcon>
            </StyledColseButtonWrapper>
          </StyledModalHeader>
          <StyledModalContent>{children}</StyledModalContent>
          {footer && <StyledModalFooter></StyledModalFooter>}
        </StyledContainer>
      </StyledModal>
    </>
  );
};

Modal.defaultProps = {
  bgColor: 'verylightpink',
};
Modal.propTypes = {
  children: PropTypes.node,
  open: PropTypes.bool,
  afterOpen: PropTypes.func,
  title: PropTypes.string,
  bgColor: PropTypes.string,
  footer: PropTypes.node,
  onClose: PropTypes.func,
};

export default Modal;
