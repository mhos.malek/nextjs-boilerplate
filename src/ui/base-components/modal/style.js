import styled, { createGlobalStyle } from 'styled-components';
import ReactModal from 'react-modal';
ReactModal.setAppElement('#__next');

const StyledModal = styled(ReactModal)`
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  display: flex;
  align-items: ${({ tall }) => (tall ? 'flex-start' : 'center')};
  justify-content: center;
  background-color: rgba(0, 0, 0, 0.8);
  overflow-x: hidden;
  overflow-y: auto;
  -webkit-overflow-scrolling: touch;
  padding: 1.2rem;
  z-index: 1031;
`;
StyledModal.displayName = 'StyledModal';

const StyledContainer = styled.div`
  background-color: ${({ theme, bgColor }) => {
    return bgColor === 'dark'
      ? '#151518'
      : theme.colors.verylightpink;
  }};

  max-width: 90%;
  min-width: 400px;
  position: relative;
  border-radius: 8px;
`;
StyledContainer.displayName = 'Container';

const StyledModalHeader = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 16px 16px;
  position: sticky;
  top: 0;
`;
const StyledTitleWrapper = styled.div``;
const StyledColseButtonWrapper = styled.div``;
const StyledModalFooter = styled.div`
  padding: 20px 0;
`;
const StyledModalContent = styled.div`
  padding: 12px 16px;
  max-height: 700px;
  overflow-y: auto;
  overflow-x: hidden;
  /* width */
  ::-webkit-scrollbar {
    width: 4px;
  }

  /* Track */
  ::-webkit-scrollbar-track {
    background: ${({ theme }) => theme.colors.darktwo};
  }

  /* Handle */
  ::-webkit-scrollbar-thumb {
    background: ${({ theme }) => theme.colors.gunmetaltwo};
  }

  /* Handle on hover */
  ::-webkit-scrollbar-thumb:hover {
    background: ${({ theme }) => theme.colors.gunmetaltwo};
  }
`;

const StyledCloseIcon = styled.div`
  cursor: pointer;
  padding: 10px;
`;

const OverRideStyles = createGlobalStyle`
.ReactModal__Overlay--after-open {
  z-index: 1000000
}
`;

export default OverRideStyles;

export {
  StyledModal,
  StyledContainer,
  StyledModalHeader,
  StyledTitleWrapper,
  StyledColseButtonWrapper,
  StyledModalFooter,
  StyledModalContent,
  StyledCloseIcon,
};
