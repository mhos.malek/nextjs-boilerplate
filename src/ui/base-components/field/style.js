import styled from 'styled-components';

const StyledField = styled.div`
  max-height: 85px;
  ${({ flex }) => flex && 'display: flex'};
  align-items: center;
  @media screen and ${({ theme }) =>
      theme.mediaQueriesMaxWidth.tablet} {
    width: 100%;
  }
`;

export default StyledField;
