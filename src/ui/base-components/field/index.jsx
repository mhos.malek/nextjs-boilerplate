import PropTypes from 'prop-types';
import StyledField from './style';
import Margins from '../margin';
const Field = ({ children, flex }) => (
  <Margins bottom="xxl">
    <StyledField flex>{children}</StyledField>{' '}
  </Margins>
);

Field.propTypes = {
  children: PropTypes.node,
  flex: PropTypes.bool,
};
export default Field;
