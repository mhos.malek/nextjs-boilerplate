import PropTypes from 'prop-types';
import {
  StyledPopover,
  StyledArrowUp,
  StyledPopoverContent,
} from './style';

const Popover = ({ children, content, disabled }) => {
  return (
    <>
      <StyledPopover disabled={disabled}>
        {children}
        <StyledPopoverContent>
          <StyledArrowUp />
          {content}
        </StyledPopoverContent>
      </StyledPopover>
    </>
  );
};

Popover.propTypes = {
  children: PropTypes.node,
  content: PropTypes.node,
  disabled: PropTypes.bool,
};

export default Popover;
