import styled from 'styled-components';

const StyledPopover = styled.div`
  background-color: ${({ theme, bg }) => theme.colors[bg]};
  position: relative;
  display: inline-block;
  transition: all 0.4s;
`;
const StyledPopoverContent = styled.div`
  opacity: 0;
  width: 86px;
  height: 39px;
  border-radius: 1px;
  background-color: black;
  color: #fff;
  text-align: center;
  padding: 5px 0;
  border-radius: 8px;
  position: absolute;
  transition: all 0.3s;
  z-index: 1;
  background-color: ${({ theme }) => theme.colors.dangersecondary};
  top: 130%;
  left: 50%;
  margin-left: -43px;
  display: flex;
  align-items: center;
  justify-content: center;
  @media screen and ${({ theme }) =>
      theme.mediaQueriesMaxWidth.tablet} {
    opacity: 1 !important;
  }
  ${({ disabled }) =>
    !disabled &&
    `${StyledPopover}:hover & {
    visibility: visible;
    opacity: 1;
    transition: all 0.3s;
  }`}
`;

const StyledArrowUp = styled.div`
  width: 0;
  height: 0;
  border-left: 5px solid transparent;
  border-right: 5px solid transparent;
  border-bottom: 5px solid
    ${({ theme }) => theme.colors.dangersecondary};
  position: absolute;
  top: -5px;
  right: 41%;
`;

export { StyledPopover, StyledArrowUp, StyledPopoverContent };
