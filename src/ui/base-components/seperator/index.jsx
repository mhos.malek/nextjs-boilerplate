/* eslint-disable react/prop-types */
import StyledSeperator from './style';

const Seperator = ({ color }) => <StyledSeperator color={color} />;

export default Seperator;
