import styled from 'styled-components';

const StyledSeperator = styled.div`
  width: 100%;
  height: 1px;
  background-color: ${({ theme, color }) =>
    color || theme.colors.darkfive};
`;

export default StyledSeperator;
