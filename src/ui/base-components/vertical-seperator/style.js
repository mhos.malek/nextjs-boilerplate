import styled from 'styled-components';

const StyledVerticalSeperator = styled.div`
  width: 1px;
  height: ${({ size }) => (size ? `${size}px` : '40px')};
  background-color: ${({ theme, color }) =>
    theme.colors[color] ||
    theme.defaultColors[color] ||
    theme.colors.gunmetal};
  display: flex;
`;

export default StyledVerticalSeperator;
