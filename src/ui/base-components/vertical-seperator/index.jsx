import StyledVerticalSeperator from './style';
import PropTypes from 'prop-types';
const VerticalSeperator = ({ size, color }) => (
  <StyledVerticalSeperator size={size} color={color} />
);

VerticalSeperator.propTypes = {
  size: PropTypes.string,
  color: PropTypes.string,
};
export default VerticalSeperator;
