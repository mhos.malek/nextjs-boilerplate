/* eslint-disable react/prop-types */
import { useRef, useEffect } from 'react';

const WithOutsideClickHandler = ({ onOutsideClick, children }) => {
  const element = useRef();
  const outsideClickHandler = (event) => {
    const isClickInside = element.current.contains(event.target);
    if (isClickInside) {
      return null;
    }
    return onOutsideClick && onOutsideClick();
  };

  useEffect(() => {
    window.addEventListener('click', outsideClickHandler);
    return function () {
      window.removeEventListener('click', outsideClickHandler);
    };
  }, []);

  return <div ref={element}>{children}</div>;
};

export default WithOutsideClickHandler;
