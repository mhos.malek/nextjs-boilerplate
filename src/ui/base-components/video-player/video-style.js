import { createGlobalStyle } from 'styled-components';

const VideoPlayerStyles = createGlobalStyle`




.vjs-fluid {
  overflow: hidden;
  outline: none;
 
}
.vjs-control-bar {
  display: flex!important;
  visibility: visible;
  opacity: 1;
  padding: 0 24px;
  bottom: -60px!important;
  background-color: rgba(0, 0, 0, 0.9)!important;
  font-family: Dana!important;

  > *:not(:first-child) {
      opacity: 0;
      transition: opacity 0.3s;
  }

}


.video-js {
  outline: none;
  &:hover {
    * {
      opacity: 1;
      transition: opacity 0.3s;
    }
  }
}
.vjs-control {
  position: absolute!important;
  &:hover {
    // .vjs-control-text {
    //   height: 35px;
    //   border-radius: 8px;
    //   background-color: rgba(0, 0, 0, 0.9);
    //   color: white;
    //   clip: inherit;
    //   min-width: 85px;
    //   width: 100%;
    //   display: flex;
    //   align-items: center;
    //   justify-content: center;
    //   z-index: 30;
    // }
  }
}

.video-js .vjs-time-control {
  font-size: 14px!important;
  font-family: Dana!important;
}

.vjs-progress-control {
  width:  100%!important;
  min-width: 100%!important;
  bottom: 28px; 
  left: 0;
  right: 0;
  padding: 0 24px!important;
  &:hover {
    .vjs-control-text { 
      display: none;
    }
   }
}

.vjs-volume-bar.vjs-slider-vertical .vjs-control-text { 
  display: none!important;
}

.vjs-play-button:before {
  font-family: inherit;
  content: 'Your text goes here';
}


.vjs-quality-selector .vjs-control-text { 
  display: none!important;
}
.vjs-progress-control .vjs-progress-holder {
  margin: 0!important;
}
.vjs-progress-holder  {
  position: absolute;
  left: 0; margin: 0;
  height: 8px; width:  100%;
}
.vjs-play-progress,
.vjs-load-progress {
  height: 8px;
}
.vjs-play-progress:before {
  font-size: 12px; top: -2px; 
  text-shadow: 0 0 2px black 
}

// left side
.vjs-play-control {
  left: 8px
}

.skip-backward-button {
  left: 40px;
}
.skip-forward-button {
  left: 72px;
}

.vjs-volume-panel {
  left: 110px;
}

.vjs-current-time  {
  display: block;
  left: 160px;
}

.vjs-duration {
  left: 200px;
  display: block;
}

.vjs-duration-display {
  :before {
    content: '/';
    margin-right: 16px;
  }
}


// right side

.vjs-fullscreen-control {
  position: absolute;
  bottom: 0; 
  right: 0;
}

.vjs-quality-selector {
  right: 32px
}

.report-button {
  right: 64px;
} 

.vjs-time-divider {
  position: absolute;
  display: block;
  left: 70px;
}



.vjs-volume-menu-button {
  position: absolute;
  bottom: 0; right: 55px;
}




// control-bar

.vjs-has-started.vjs-user-inactive.vjs-playing .vjs-control-bar {
  opacity: 1;
}

.vjs-user-inactive .vjs-play-progress {
  opacity: 1;
}

.video-js .vjs-progress-holder .vjs-play-progress {
    ${({ theme }) =>
      `background-color: ${theme.defaultColors.primary}`};
    height: 4px;
    border-radius: 2px;
}






  /* Change the border of the big play button. */
  .vjs-big-play-button {
   display: none!important;
  }
  

  .video-js .vjs-time-control {
      display:block;
    }  
  

//   quailty selector 

.vjs-menu-button-popup .vjs-menu .vjs-menu-content {
    border-radius: 8px;
    background-color: #000000;
    padding: 10px 0;
}

.vjs-menu li {
    text-align: left;
    padding: 2px 10px;
}

.vjs-menu li.vjs-menu-title {
    display: none;
}

.video-js .vjs-play-progress:before {
  color:  ${({ theme }) => theme.defaultColors.primary};
  font-size: 16px;
}

.vjs-menu li.vjs-selected, .vjs-menu li.vjs-selected:focus, .vjs-menu li.vjs-selected:hover, .vjs-menu li.vjs-selected:hover {
    ${({ theme }) =>
      `color: ${theme.defaultColors.secondary}`}!important;
      background-color: transparent!important;
      color: white;
      transition: all 0.3s;
      &::after {
            width: 8px!important;
          height: 8px!important;
          border-radius: 50%!important;
          ${({ theme }) =>
            `background-color: ${theme.defaultColors.primary}`};
            margin-left: 8px!important;

      }
}
.vjs-menu li.vjs-menu-item:hover {
    ${({ theme }) =>
      `background-color: ${theme.defaultColors.secondary}`};
      transition: all 0.3s;
}


//   volume section
  .video-js .vjs-volume-vertical {
      background-color: transparent;
      bottom: 120px;
  }

  .vjs-matrix .vjs-volume-level {
    ${({ theme }) =>
      `background-color: ${theme.defaultColors.primary}`};
      border-bottom-right-radius: 12px;
      border-bottom-left-radius: 12px;

  }
  .vjs-slider-vertical .vjs-volume-level {
    ${({ theme }) => `width: ${theme.dimensions.lg}`};
  
  }

  .video-js .vjs-volume-level:before {
      content: none;
  }

  .video-js .vjs-volume-panel .vjs-volume-control.vjs-volume-vertical {
    height: 96px;
    left: 0!important;
    bottom: 90px;
    margin-left: 6px!important;
  }

  .vjs-volume-bar.vjs-slider-vertical {
    background: #232323;
    width: 16px;
    height: 96px;
    margin: 1.35em auto;
    border-radius: 12px;
    border-radius: 12px;
    overflow: hidden;
    outline: none;
}



  /* Change the color of various "bars". */
  .vjs-matrix .vjs-volume-level,
  .vjs-matrix .vjs-play-progress,
  .vjs-matrix .vjs-slider-bar {
    // background: #00ff00;
  }
// fullscreen

.vjs-fullscreen {
  .vjs-control-bar {
    bottom: 30px!important;
  }
}

// change Icons

.vjs-icon-play:before, .video-js .vjs-play-control .vjs-icon-placeholder:before, .video-js .vjs-big-play-button .vjs-icon-placeholder:before {
  content: "\\e923"!important;
  font-family: PF-icons;
}
}
.video-js .vjs-play-control.vjs-playing .vjs-icon-placeholder:before {
      content: "\\e921"!important;
      font-family: PF-icons;
    }


.vjs-icon-backward span {
  ::before {
    content: "\\e910"!important;
  font-family: PF-icons;
  }
}

.vjs-icon-skipforward span {
  ::before {
    content: "\\e91b"!important;
  font-family: PF-icons;
  }
}

.vjs-icon-report  span {
  ::before {
    content: "\\e91a"!important;
  font-family: PF-icons;
  }
}

.vjs-icon-volume-high:before, .video-js .vjs-mute-control .vjs-icon-placeholder:before {
  content: "\\e931"!important;
  font-family: PF-icons;
}

.vjs-fullscreen-control[title="Non-Fullscreen"] {
  span:before {
    content: "\\e927"!important;
    font-family: PF-icons;
  }
}

  .vjs-fullscreen-control[title="Fullscreen"] {
    span:before {
      content: "\\e928"!important;
      font-family: PF-icons;
    }
  }

// scrollbar

.vjs-menu-button-popup, .vjs-menu, .vjs-menu-content {
  font-family: Dana!important;
  overflow: hidden;
  /* Track */
  ::-webkit-scrollbar-track {
    background: ${({ theme }) => theme.colors.darktwo}!important;
  }

  /* Handle */
  ::-webkit-scrollbar-thumb {
    background: ${({ theme }) => theme.colors.gunmetaltwo}!important;
  }

  /* Handle on hover */
  ::-webkit-scrollbar-thumb:hover {
    background: ${({ theme }) => theme.colors.gunmetaltwo}!important;
  }
}
`;

export default VideoPlayerStyles;
