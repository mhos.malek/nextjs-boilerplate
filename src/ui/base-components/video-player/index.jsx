/* eslint-disable react/prop-types */
import { useEffect, useRef, useState } from "react";
import videojs from "video.js";
require("videojs-contrib-quality-levels");
require("videojs-hls-quality-selector");
import VideoPlayerStyles from "./video-style";
import PropTypes from "prop-types";
import { StyledVideoContainer, StyledVideoLibWrapper } from "./style";
import { useRouter } from "next/router";

let Button = videojs.getComponent("Button");

const VideoPlayer = ({ getPlayerInstance, videoData }) => {
  const [player, setPlayer] = useState();
  const videoNode = useRef(null);
  const router = useRouter();
  const { query } = router;

  const videoJsOptions = {
    autoplay: false,
    controls: true,
    fill: true,
    crossOrigin: "Anonymous",
    withCredentials: false,
    controlBar: {
      volumePanel: {
        inline: false,
      },

      children: [
        "progressControl",
        "playToggle",
        "volumePanel",
        "currentTimeDisplay",
        "durationDisplay",
        "fullscreenToggle",
      ],
    },
    sources: [
      {
        src: videoData.src || null,
        type: "application/x-mpegURL",
        withCredentials: false,
      },
    ],
  };

  const addQualityLevelFunctionality = () => {
    const addQualityButton = () => {
      player.hlsQualitySelector({
        displayCurrentQuality: true,
      });
    };
    let qualityLevels = player.qualityLevels();
    qualityLevels.on("addqualitylevel", function (event) {
      return event;
    });
    addQualityButton();
  };

  const handlePlayerEventListeners = () => {
    const onVideoCompleted = () => {
      setShowCommentVideo(true);
      setShowNextEpisode(false);
    };
    let isShown = false;
    const ontimeupdate = (player) => {
      const videoDuration = player.duration();
      const currentTime = player.currentTime();
      if (currentTime > videoDuration - 10) {
        if (!isShown) {
          setShowNextEpisode(true);
          isShown = true;
        }
      } else {
        if (isShown) {
          setShowNextEpisode(false);
          isShown = false;
        }
      }
    };

    player.ready(() => {
      player.play();
      player.bigPlayButton.hide();
    });

    player.on("mouseout", function () {});

    player.on("mouseover", function () {});

    player.on("play", function () {
      setIsPlaying(true);
    });
    player.on("pause", function () {
      setIsPlaying(false);
    });

    player.on("seeked", () => console.log("malekkk"));
    player.on("seeking", () => {
      setShowCommentVideo(false);
    });

    player.on("ended", () => onVideoCompleted());

    player.on("timeupdate", function () {
      ontimeupdate(this);
    });
  };

  const initVideoPlayer = () => {
    // videojs.registerPlugin('qualityLevels', contribQualityLevels);
    let playerInstance = videojs(
      videoNode.current,
      videoJsOptions,
      function onPlayerReady() {}
    );
    setPlayer(playerInstance);
    getPlayerInstance(playerInstance);
  };

  const createSkipButton = (time = 30) => {
    const addSkipFoward = () => {
      const skipFrowardButton = videojs.extend(Button, {
        constructor: function () {
          Button.apply(this, arguments);
          this.controlText("skip forward");
          this.addClass("vjs-icon-skipforward");
          this.addClass("skip-forward-button");
        },
        handleClick: function () {
          const currentTime = this.player().currentTime();
          this.player().currentTime(currentTime + time);
        },
      });

      videojs.registerComponent("skipFrowardButton", skipFrowardButton);
      player.getChild("controlBar").addChild("skipFrowardButton", {});
    };

    const addSkipBackward = () => {
      const skipBackwardButton = videojs.extend(Button, {
        constructor: function () {
          Button.apply(this, arguments);
          this.controlText("skip backward");
          this.addClass("vjs-icon-backward");
          this.addClass("skip-backward-button");
        },
        handleClick: function () {
          const currentTime = this.player().currentTime();
          this.player().currentTime(currentTime - time);
        },
      });
      videojs.registerComponent("skipBackwardButton", skipBackwardButton);
      player.getChild("controlBar").addChild("skipBackwardButton", {});
    };
    addSkipBackward();
    addSkipFoward();
  };

  const createFlagButton = () => {
    const flagButton = videojs.extend(Button, {
      constructor: function () {
        Button.apply(this, arguments);
        this.controlText("Report");
        this.addClass("vjs-icon-report");
        this.addClass("report-button");
      },
      handleClick: function () {
        //   onFlagClick
        console.log("flag click");
      },
    });
    videojs.registerComponent("flagButton", flagButton);
    player.getChild("controlBar").addChild("flagButton", {});
  };

  useEffect(() => {
    initVideoPlayer();
    return function () {
      if (player) {
        player.dispose();
      }
    };
  }, []);

  useEffect(() => {
    if (player) {
      player.addClass("vjs-matrix");
      addQualityLevelFunctionality();
      handlePlayerEventListeners();
      createSkipButton();
      createFlagButton();
    }
  }, [player]);

  return (
    <>
      <StyledVideoLibWrapper>
        <StyledVideoContainer>
          <VideoPlayerStyles />

          <div data-vjs-player>
            <video ref={videoNode} className="video-js"></video>
          </div>
        </StyledVideoContainer>
      </StyledVideoLibWrapper>
    </>
  );
};

VideoPlayer.propTypes = {
  getPlayerInstance: PropTypes.func,
};
export default VideoPlayer;
