import styled from 'styled-components';

const StyledVideoContainer = styled.div`
  position: relative;
  width: 100%;
  height: 495px;
  opacity: ${({ loading }) => (loading ? 0 : 1)};
  transition: opacity 0.3s;
`;

const StyledNextVideo = styled.div`
  position: absolute;
  right: 0;
  top: 0;
  bottom: 0;
  margin: auto;
  z-index: 3;
  height: 56px;
  overflow: hidden;
  display: flex;
  cursor: pointer;
`;

const StyledNextIcon = styled.div`
  height: 56px;
  width: 56px;
  background-color: rgba(0, 0, 0, 0.74);
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 3;
  opacity: 0;
  transition: all 0.3s;
  &:hover {
    transition: all 0.3s
    border-top-left-radius: 0px;
    border-bottom-left-radius: 0px;
  }
  ${StyledVideoContainer}:hover & {
    opacity: 1;
    transition: all 0.3s;
  }
`;

const StyledNextItemContent = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 4px 16px;
  border-right: 1px solid #494955;
  background-color: rgba(0, 0, 0, 0.74);
  height: 56px;
  border-top-left-radius: 8px;
  border-bottom-left-radius: 8px;
  transform: translateX(calc(100% + 56px));
  ${StyledNextVideo}:hover & {
    transform: translateX(0);
  }

  transition: all 0.3s ease-in-out;
`;

const StyledPrevVideo = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  bottom: 0;
  margin: auto;
  z-index: 3;
  height: 56px;
  overflow: hidden;
  display: flex;
  cursor: pointer;
`;
const StyledPrevIcon = styled.div`
height: 56px;
width: 56px;
background-color: rgba(0, 0, 0, 0.74);
display: flex;
align-items: center;
justify-content: center;
z-index: 3;
opacity: 0;
transition: all 0.3s;
&:hover {
  transition: all 0.3s
  border-top-right-radius: 0px;
  border-bottom-right-radius: 0px;
}
${StyledVideoContainer}:hover & {
  opacity: 1;
  transition: all 0.3s;
}
`;

const StyledPrevItemContent = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 4px 16px;
  border-left: 1px solid #494955;
  background-color: rgba(0, 0, 0, 0.74);
  height: 56px;
  border-top-right-radius: 8px;
  border-bottom-right-radius: 8px;
  transform: translateX(calc(-100% - 56px));
  ${StyledPrevVideo}:hover & {
    transform: translateX(0);
  }

  transition: all 0.3s ease-in-out;
`;

const StyledNextEpisodeModal = styled.div`
  position: absolute;
  right: 24px;
  bottom: 30px;
  z-index: 3;
  background-color: rgba(0, 0, 0, 0.74);
  cursor: pointer;
  min-width: 300px;
  padding: ${({ theme }) => theme.dimensions.xl};
  border-radius: ${({ theme }) => theme.dimensions.md};
`;

const StyledNextEpisodeHeader = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const StyledNextEpisodeTitle = styled.div``;

const StyledEndOfVideModal = styled.div`
  position: absolute;
  right: 0;
  left: 0;
  top: 0;
  bottom: 0;
  margin: auto;
  z-index: 3;
  width: 330px;
  height: 168px;
  background-color: rgba(0, 0, 0, 0.74);
  padding: ${({ theme }) => theme.dimensions.xl};
  border-radius: ${({ theme }) => theme.dimensions.md};
`;

const StyledEndOfVideModalHeader = styled.div`
  text-align: left;
  cursor: pointer;
`;

const StyledEndOfVideModalContent = styled.div`
  text-align: center;
  paddin-bottom: 10px;
`;

const StyledIconWrapper = styled.div`
  display: flex;
  aling-items: center;
  justify-content: center;
`;

const StyledIcon = styled.div`
  width: 48px;
  height: 48px;
  border-radius: 50%;
  background-color: ${({ theme }) => theme.colors.darktwo};
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: 10px;
  cursor: pointer;
  span {
    color: ${({ isActive, thumbsDown, thumbsUp, theme }) => {
      if (isActive && thumbsDown) {
        return theme.colors.grapefruit;
      }
      if (isActive && thumbsUp) return theme.colors.deepskyblue;
      return null;
    }}};
  }

  &:hover {
    span {
      color: ${({ theme, thumbsDown }) =>
        thumbsDown
          ? theme.colors.grapefruit
          : theme.colors.deepskyblue};
    }
  }
`;

const StyledCustomPlayToggleButton = styled.div`
  position: absolute;
  right: 0;
  left: 0;
  top: 0;
  bottom: 0;
  margin: auto;
  ${({ theme }) => `background-color: ${theme.colors.black70}`};
  width: 96px;
  height: 96px;
  border-radius: 50%;
  border: none;
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 2;
  opacity: 0;
  transition: all 0.3s;
  cursor: pointer;
  ${StyledVideoContainer}:hover & {
    opacity: 1;
    transition: all 0.3s;
  }
`;

const StyledVideoLibWrapper = styled.div`
  opacity: ${({ loading }) => (loading ? 0 : 1)};
  transition: all 0.3s;
`;
const StyledLoadingWrapper = styled.div`
  // opacity: ${({ loading }) => (loading ? 1 : 0)};
`;

export {
  StyledVideoContainer,
  StyledNextVideo,
  StyledNextItemContent,
  StyledPrevVideo,
  StyledPrevItemContent,
  StyledNextIcon,
  StyledPrevIcon,
  StyledNextEpisodeModal,
  StyledNextEpisodeHeader,
  StyledNextEpisodeTitle,
  StyledEndOfVideModal,
  StyledEndOfVideModalHeader,
  StyledEndOfVideModalContent,
  StyledIconWrapper,
  StyledIcon,
  StyledCustomPlayToggleButton,
  StyledVideoLibWrapper,
  StyledLoadingWrapper,
};
