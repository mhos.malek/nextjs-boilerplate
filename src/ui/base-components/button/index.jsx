import PropTypes from 'prop-types';
import {
  StyledButton,
  StyledLoadingWithText,
  StyledTextWrapper,
  StyledLoadingWrapper,
  StyledIconWrapper,
} from './style';
import Icon from '../icon';
import Loading from '../loading';
const Button = ({
  children,
  disabled,
  onClick,
  size,
  background,
  color,
  outline,
  type,
  height,
  loading,
  icon,
}) => {
  const renderButtonText = () => {
    if (loading) {
      if (size !== 'full') {
        return <Loading />;
      }
      return (
        <StyledLoadingWithText>
          <StyledTextWrapper> {children} </StyledTextWrapper>
          <StyledLoadingWrapper>
            <Loading />
          </StyledLoadingWrapper>
        </StyledLoadingWithText>
      );
    }
    return children;
  };
  return (
    <StyledButton
      onClick={onClick ? () => onClick() : null}
      disabled={disabled}
      size={size}
      background={background}
      color={color}
      outline={outline}
      type={type}
      height={height}
    >
      {icon && (
        <StyledIconWrapper>
          <Icon
            name={icon.name}
            color={icon.color || color || 'verylightpink'}
          />
        </StyledIconWrapper>
      )}
      {renderButtonText()}
    </StyledButton>
  );
};
Button.defaultProps = {
  height: '44px',
};
Button.propTypes = {
  children: PropTypes.node,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  size: PropTypes.string,
  background: PropTypes.string,
  color: PropTypes.string,
  outline: PropTypes.string,
  type: PropTypes.string,
  height: PropTypes.string,
  loading: PropTypes.bool,
  icon: PropTypes.any,
};

export default Button;
