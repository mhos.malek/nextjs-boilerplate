import styled from 'styled-components';
const generateThemeBasedOnType = (type, theme) => {
  if (type === 'primary') {
    return `
    background-color: ${theme.defaultColors.primary};
    color: ${theme.colors.white};
    `;
  }
  if (type === 'outline-primary') {
    return `
    background-color: ${theme.colors.transparent};
    color: ${theme.defaultColors.primary};
    border: 1px solid ${theme.defaultColors.primary};
    `;
  }
  if (type === 'outline-secondary') {
    return `
    background-color: ${theme.colors.transparent};
    color: ${theme.defaultColors.secondary};
    border: 1px solid ${theme.defaultColors.secondary};
    `;
  }

  if (type === 'outline-white') {
    return `
    background-color: ${theme.colors.transparent};
    color: ${theme.colors.white};
    border: 1px solid ${theme.colors.white};
    `;
  }
  if (type === 'circle-primary') {
    return `
    background-color: ${theme.defaultColors.primary};
    color: ${theme.colors.verylightpink};
    border: 0!important;
    border-radius: 50%;

    `;
  }
  if (type === 'outline-dark') {
    return `
    background-color: ${theme.colors.transparent};
    color: ${theme.colors.darktwo};
    border: 1px solid ${theme.colors.darktwo};
    `;
  }

  if (type === 'dark-bg') {
    return `
    background-color: ${theme.colors.darkthree};
    color: ${theme.colors.verylightpink};
    border: 0!important;
    `;
  }
  if (type === 'dark-five') {
    return `
    background-color: ${theme.colors.darkfive};
    color: ${theme.colors.verylightpink};
    border: 0!important;
    `;
  }
  if (type === 'bg-white') {
    return `
    background-color: ${theme.colors.white};
    color: #000000;
    border-radius: 16px;
    border: 0!important;
    `;
  }
  if (type === 'disabled') {
    return `
    background-color: ${theme.colors.darkfive};
    color: ${theme.colors.greyishbrowntwo};
    border: 0!important;
    `;
  }
  if (type === 'glass-white') {
    return `
    background-color: rgba(255, 255, 255, 0.3);
    color: ${theme.colors.white};
    border: 0!important;
    padding: 8px 32px;
    `;
  }
};
const StyledButton = styled.button`
${({ theme }) => theme.typography.defaultFont.normal[16]};
  width: ${({ theme, size }) => theme.dimensions[size] || size};
  border-radius: ${({ theme }) => theme.dimensions.md};
  height: ${({ theme, height }) =>
    theme.dimensions[height] || height || '44px'};
  display: flex;
  align-items: center;
  justify-content: center;
  outline: none;
  cursor: pointer;
  font-family: Dana;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  border: none;
  padding-top: 4px;
  @media screen and ${({ theme }) =>
    theme.mediaQueriesMaxWidth.tablet} {
      ${({ theme }) => theme.typography.defaultFont.normal[13]};
}

  ${({ type, theme }) =>
    type && generateThemeBasedOnType(type, theme)};
    ${({ disabled, theme }) =>
      disabled && generateThemeBasedOnType('disabled', theme)}
  background-color: ${({ theme, background }) =>
    theme.colors[background] || theme.defaultColors[background]};
  background-color: ${({ theme, disabled, background }) =>
    disabled
      ? theme.colors.darkfive
      : theme.colors[background] || theme.defaultColors[background]};
  color: ${({ theme, color }) =>
    theme.colors[color] || theme.defaultColors[color]};
  color: ${({ theme, color, disabled }) =>
    disabled
      ? theme.colors.greyishbrowntwo
      : theme.colors[color] || theme.defaultColors[color]};
    ${({ theme, outline }) =>
      outline &&
      ` border: 1px solid ${
        theme.defaultColors[outline] || theme.colors[outline]
      }`}
        `;
const StyledLoadingWithText = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
`;

const StyledTextWrapper = styled.div`
  margin-right: auto;
  margin-left: -40px;
`;
const StyledLoadingWrapper = styled.div`
  margin-right: auto;
  margin-left: 16px;
`;

const StyledIconWrapper = styled.div`
  margin: 0 8px;
  margin-top: 8px;
  @media screen and ${({ theme }) =>
      theme.mediaQueriesMaxWidth.tablet} {
    display: none;
  }
`;

export {
  StyledButton,
  StyledLoadingWithText,
  StyledTextWrapper,
  StyledLoadingWrapper,
  StyledIconWrapper,
};
