import { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  StyledWrapper,
  StyledInputContainer,
  StyledReadOnly,
} from './style';

const ReadOnlyInput = ({
  name,
  onFocus,
  onBlur,
  value,
  direction,
  getInputRef,
}) => {
  const inputRef = useRef(null);
  const handleOnBlur = (e) => onBlur && onBlur(e);
  const handleOnFocus = (e) => onFocus && onFocus(e);

  useEffect(() => {
    if (inputRef.current) {
      getInputRef && getInputRef(inputRef.current);
    }
  }, [inputRef]);

  return (
    <StyledWrapper>
      <StyledInputContainer>
        <StyledReadOnly
          direction={direction}
          name={name}
          disabled
          ref={inputRef}
          value={value}
          onBlur={handleOnBlur}
          onFocus={handleOnFocus}
        />
      </StyledInputContainer>
    </StyledWrapper>
  );
};
ReadOnlyInput.defaultProps = {
  direction: 'rtl',
};
ReadOnlyInput.propTypes = {
  name: PropTypes.string,
  type: PropTypes.string,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  value: PropTypes.string,
  direction: PropTypes.string,
  getInputRef: PropTypes.func,
};
export default ReadOnlyInput;
