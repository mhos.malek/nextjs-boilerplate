import styled from 'styled-components';

const StyledWrapper = styled.div`
  width: 100%;
`;
const StyledLabel = styled.div`
  margin-bottom: ${({ theme }) => theme.dimensions.md};
  color: ${({ theme, disabled }) =>
    disabled ? '#565656' : theme.colors.browngrey};
`;
const StyledInputContainer = styled.div`
  position: relative;
  height: 40px;
  @media screen and ${({ theme }) =>
      theme.mediaQueriesMaxWidth.tablet} {
    ${({ isSelectable }) => isSelectable && `margin-right: -35px`}
  }
`;
const StyledInput = styled.input`
  background-color: ${({ theme }) => theme.colors.darkfive};
  border: 1px solid transparent;
  width: 100%;
  height: 100%;
  outline: none;
  text-align: ${({ align }) => (align ? align : 'center')};
  font-family: Dana;
  padding-left: 16px;
  padding-right: ${({ align }) =>
    align === 'right' ? '44px' : '16px'};
  border-radius: ${({ theme }) => theme.dimensions.sm};
  color: ${({ theme }) => theme.colors.verylightpink};
  direction: ${({ direction }) => direction};
  transition: all 0.3s;
  ${({ theme }) => theme.typography.defaultFont.regular[13]};
  ${({ hasError, theme }) =>
    hasError
      ? `border-color: ${theme.defaultColors.danger}!important;`
      : null}
  &:placeholder {
    color: ${({ theme }) => theme.colors.browngrey};
  }
  &:disabled {
    &::placeholder {
      color: #565656;
    }
  }
  &:focus {
    border-color: ${({ theme }) => theme.defaultColors.primary};
    transition: all 0.3s;
  }
`;

const StyledIcon = styled.div`
  position: absolute;
  right: ${({ theme }) => theme.dimensions.lg};
  height: 100%;
  display: flex;
  align-items: center;
  color: ${({ theme }) => theme.colors.browngrey};
}
`;

const StyledClearIconWrapper = styled.div`
  width: 20px;
  height: 100%;
  position: absolute;
  left: ${({ theme }) => theme.dimensions.lg};
  display: flex;
  align-items: center;
`;

const StyledClearIcon = styled.div`
  position: relative;
  width: 20px;
  height: 20px;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  display: flex;
  align-items: center;
  background-color: ${({ theme }) => theme.defaultColors.dark};
  transform: scale(${({ show }) => (show ? '1' : '0')});
  transition: all 0.3s;
  span {
    position: absolute;
    top: 6.4px;
    right: 5px;
  }
`;

const StyledShowPasswordIcon = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  height: 100%;
  display: flex;
  align-items: center;
  left: ${({ theme }) => theme.dimensions.lg};
  transform: scale(${({ show }) => (show ? '1' : '0')});
  transition: all 0.3s;
  padding-left: 2px;
`;

const StyledOtp = styled.div`
  direction: ltr;
  > div > div {
    flex: 1;
    margin-right: 15px;
    &:last-child {
      margin-right: 0;
    }
  }
  input {
    background-color: ${({ theme }) => theme.colors.darkfive};
    border: 1px solid transparent;
    width: 40px;
    height: 40px;
    text-align: center;
    font-size: 13px;
    font-family: Dana;
    border-radius: ${({ theme }) => theme.dimensions.sm};
    color: ${({ theme }) => theme.colors.verylightpink};
    flex: 1;
    outline: none;

    @media screen and ${({ theme }) =>
        theme.mediaQueriesMinWidth.tablet} {
      padding-left: 14px;
    }

    &:not([value='']) {
      border-color: ${({ theme }) => theme.defaultColors.primary};
    }
    &:focus {
      border-color: ${({ theme }) => theme.defaultColors.primary};
    }
    border-color: ${({ hasError, theme }) =>
      hasError && theme.defaultColors.danger}!important;
    transition: all 0.3s;
  }
`;

const StyledReadOnly = styled.input`
  background-color: ${({ theme }) => theme.colors.transparent};
  border: 1px solid ${({ theme }) => theme.colors.darktwo};
  width: 100%;
  height: 40px;
  outline: none;
  text-align: center;
  font-family: Dana;
  padding-left: 16px;
  padding-right: 16px;
  border-radius: ${({ theme }) => theme.dimensions.sm};
  color: ${({ theme }) => theme.colors.darktwo};
  direction: ${({ direction }) => direction};
  transition: all 0.3s;
  ${({ theme }) => theme.typography.defaultFont.regular[13]};

  &:focus {
    border-color: ${({ theme }) => theme.defaultColors.primary};
    transition: all 0.3s;
  }
`;

export {
  StyledWrapper,
  StyledLabel,
  StyledInputContainer,
  StyledInput,
  StyledIcon,
  StyledClearIcon,
  StyledShowPasswordIcon,
  StyledOtp,
  StyledClearIconWrapper,
  StyledReadOnly,
};
