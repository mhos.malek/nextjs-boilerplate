import { useRef, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  StyledWrapper,
  StyledLabel,
  StyledInputContainer,
  StyledInput,
  StyledIcon,
  StyledClearIcon,
  StyledClearIconWrapper,
} from './style';
import Typography from '../typography';
import validatorFunctions from '../../../services/validation/validator-functions';
import Margins from '../margin';
import Icon from '../icon';
import { validationTypes } from '../../../services/validation/types';

const Input = ({
  name,
  type,
  placeholder,
  label,
  onChange,
  disabled,
  isSelectable,
  isSelected,
  onFocus,
  onBlur,
  customIcon,
  value,
  direction,
  validation,
  helperText,
  errorMessage,
  hasError,
  hasClearIcon,
  align,
}) => {
  const [showClearIcon, setShowClearIcon] = useState(false);
  const [valueState, setValueState] = useState(value);
  const [hasValidationError, setHasValidationError] = useState(false);
  const inputRef = useRef(null);
  const handleOnBlur = (e) => onBlur && onBlur(e);
  const handleOnFocus = (e) => onFocus && onFocus(e);
  const handleOnChange = ({ target }) => {
    const { value } = target;
    onChange && onChange(name, value);
  };
  const handleOnClearClick = () => {
    onChange(name, '');
  };
  const handleValidation = (value) => {
    // ustom function
    if (!validation.validateFromThisLength)
      validation.validateFromThisLength = 1;
    if (validation.type === validationTypes.customFunction) {
      if (
        value.length >= validation.validateFromThisLength &&
        !validation.validateFunction()
      ) {
        validation &&
          validation.onError &&
          validation.onError(name, true);
        return setHasValidationError(true);
      }
      value.length > validation.validateFromThisLength &&
        validation &&
        validation.onError &&
        validation.onError(name, false);
      return setHasValidationError(false);
    }

    // pre defined validation
    if (
      value.length >= validation.validateFromThisLength &&
      !validatorFunctions[validation.type](
        value,
        validation.parameter
      )
    ) {
      value.length >= validation.validateFromThisLength &&
        validation &&
        validation.onError &&
        validation.onError(name, true);
      return setHasValidationError(true);
    }
    value.length >= validation.validateFromThisLength &&
      validation &&
      validation.onError &&
      validation.onError(name, false);
    return setHasValidationError(false);
  };

  const renderInputBottomText = () => {
    if (hasValidationError) {
      return (
        <Margins top="sm">
          <Typography type="regular" size="12" color="danger">
            {validation && validation.errorMessage}
          </Typography>
        </Margins>
      );
    }
    if (hasError) {
      return (
        <Margins top="sm">
          <Typography type="regular" size="12" color="danger">
            {errorMessage}
          </Typography>
        </Margins>
      );
    }
    if (helperText) {
      return (
        <Margins top="sm">
          <Typography
            type="regular"
            size="12"
            color="greyishbrowntwo"
          >
            {helperText}
          </Typography>
        </Margins>
      );
    }
    return null;
  };

  useEffect(() => {
    setValueState(value);
    if (validation) {
      handleValidation(value);
    }
    if (value !== '' || value.length > 0) {
      setShowClearIcon(true);
    } else {
      setShowClearIcon(false);
    }
  }, [value]);

  return (
    <StyledWrapper>
      {label && (
        <StyledLabel>
          <Typography
            size="14"
            type="regular"
            color={disabled ? '#565656' : 'browngrey'}
          >
            {label}
          </Typography>
        </StyledLabel>
      )}
      <StyledInputContainer isSelectable={isSelectable}>
        {customIcon && (
          <StyledIcon disabled={disabled}>{customIcon}</StyledIcon>
        )}
        {hasClearIcon && (
          <StyledClearIconWrapper onClick={handleOnClearClick}>
            <StyledClearIcon show={showClearIcon}>
              <Icon name="Delete" size="8" />
            </StyledClearIcon>
          </StyledClearIconWrapper>
        )}
        <StyledInput
          direction={direction}
          name={name}
          type={type}
          placeholder={placeholder}
          disabled={disabled}
          ref={inputRef}
          value={valueState}
          onBlur={handleOnBlur}
          onFocus={handleOnFocus}
          onChange={handleOnChange}
          hasError={hasValidationError || hasError}
          isSelected={isSelected}
          align={align}
          hasCustomIcon={customIcon ? true : false}
        />
      </StyledInputContainer>
      {renderInputBottomText()}
    </StyledWrapper>
  );
};
Input.defaultProps = {
  type: 'text',
  disabled: false,
  direction: 'rtl',
  hasClearIcon: false,
  align: 'center',
};
Input.propTypes = {
  name: PropTypes.string,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  label: PropTypes.string,
  customIcon: PropTypes.node,
  disabled: PropTypes.bool,
  isSelectable: PropTypes.bool,
  isSelected: PropTypes.bool,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  onChange: PropTypes.func,
  value: PropTypes.string,
  direction: PropTypes.string,
  validation: PropTypes.object,
  helperText: PropTypes.bool,
  hasError: PropTypes.bool,
  errorMessage: PropTypes.string,
  hasClearIcon: PropTypes.bool,
  align: PropTypes.string,
};
export default Input;
