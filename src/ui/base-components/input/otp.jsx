import PropTypes from 'prop-types';
import OtpInput from 'react-otp-input';
import { StyledOtp } from './style';
import { useEffect, useState } from 'react';
import Typography from '../typography';
import Margins from '../margin';

const Otp = ({
  onChange,
  numInputs,
  isInputNum,
  hasError,
  value,
  onCompleted,
  errorMessage,
}) => {
  // eslint-disable-next-line no-unused-vars
  const [isCompleted, setIsCompleted] = useState(false);
  useEffect(() => {
    if (value && value.toString().length === numInputs) {
      onCompleted && onCompleted(true);
      return setIsCompleted(true);
    }
    return setIsCompleted(false);
  }, [value]);

  const renderInputBottomText = () => {
    if (hasError) {
      return (
        <Margins top="sm">
          <Typography type="regular" size="12" color="danger">
            {errorMessage}
          </Typography>
        </Margins>
      );
    }
    return null;
  };

  return (
    <StyledOtp hasError={hasError}>
      <OtpInput
        onChange={(otp) => onChange(otp)}
        value={value}
        numInputs={numInputs}
        isInputNum={isInputNum}
        hasErrored={hasError}
      />
      {renderInputBottomText()}
    </StyledOtp>
  );
};
Otp.defaultProps = {
  isInputNum: true,
  numInputs: 6,
  hasError: false,
};
Otp.propTypes = {
  onChange: PropTypes.func,
  numInputs: PropTypes.number,
  hasError: PropTypes.bool,
  isInputNum: PropTypes.bool,
  value: PropTypes.node,
  onCompleted: PropTypes.func,
  errorMessage: PropTypes.string,
};

export default Otp;
