import SwipeableViews from 'react-swipeable-views';
import PropTypes from 'prop-types';

const SwipeableView = ({ children }) => (
  <SwipeableViews enableMouseEvents>{children}</SwipeableViews>
);

SwipeableView.propTypes = { children: PropTypes.node };

export default SwipeableView;
