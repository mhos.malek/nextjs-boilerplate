import ReactCarousel from '@brainhubeu/react-carousel';
import PropTypes from 'prop-types';
import {
  StyledCarousel,
  StyledArrow,
  StyledDot,
  StyledDotWrapper,
  StyledLeftArrowWrapper,
  StyledRightArrowWrapper,
} from './style';
import Icon from '../icon';
import { useState } from 'react';
const Carousel = ({
  children,
  slidesPerPage,
  ArrowComponent,
  infinite,
  CustomDots,
}) => {
  const [value, setValue] = useState(0);
  const onChange = (value) => {
    setValue(Number(value));
  };
  const renderDots = () => {
    const numberOfDots = children.length - slidesPerPage;
    const childrens = [];
    for (var i = 0; i <= numberOfDots; i++) {
      if (CustomDots) {
        childrens.push(
          <CustomDots
            data-index={i}
            onClick={(e) => {
              onChange(e.target.dataset.index);
            }}
            key={i}
            active={value === i}
          ></CustomDots>
        );
      } else {
        childrens.push(
          <StyledDot
            data-index={i}
            onClick={(e) => {
              onChange(e.target.dataset.index);
            }}
            key={i}
            active={value === i}
          ></StyledDot>
        );
      }
    }
    return <StyledDotWrapper>{childrens}</StyledDotWrapper>;
  };

  return (
    <>
      <StyledCarousel>
        <ReactCarousel
          slidesPerPage={slidesPerPage}
          arrows
          rtl
          infinite={infinite}
          breakpoints={{
            640: {
              slidesPerPage: 1,
              arrows: false,
            },
            900: {
              slidesPerPage: 2,
              arrows: false,
            },
          }}
          arrowLeftDisabled={
            <StyledLeftArrowWrapper
              onClick={() => onChange(value - 1)}
            >
              {ArrowComponent ? (
                <ArrowComponent>
                  <Icon
                    name="Arrow-Down"
                    rotate={270}
                    size="10"
                    color="#878787"
                  />
                </ArrowComponent>
              ) : (
                <StyledArrow>
                  <Icon
                    name="Arrow-Down"
                    rotate={270}
                    size="10"
                    color="#878787"
                  />
                </StyledArrow>
              )}
            </StyledLeftArrowWrapper>
          }
          arrowLeft={
            <StyledLeftArrowWrapper
              onClick={() => onChange(value - 1)}
            >
              {ArrowComponent ? (
                <ArrowComponent>
                  <Icon
                    name="Arrow-Down"
                    rotate={270}
                    size="10"
                    color="#878787"
                  />
                </ArrowComponent>
              ) : (
                <StyledArrow>
                  <Icon
                    name="Arrow-Down"
                    rotate={270}
                    size="10"
                    color="#878787"
                  />
                </StyledArrow>
              )}
            </StyledLeftArrowWrapper>
          }
          arrowRight={
            <StyledRightArrowWrapper
              onClick={() => {
                if (!infinite) {
                  if (value < children.length - slidesPerPage) {
                    onChange(value + 1);
                  }
                } else {
                  if (value < children.length - slidesPerPage) {
                    onChange(value + 1);
                  }
                  if (value === children.length - 1) {
                    onChange(0);
                  }
                }
              }}
            >
              {ArrowComponent ? (
                <ArrowComponent>
                  <Icon
                    name="Arrow-Down"
                    rotate={-270}
                    size="10"
                    color="#878787"
                  />
                </ArrowComponent>
              ) : (
                <StyledArrow>
                  <Icon
                    name="Arrow-Down"
                    rotate={-270}
                    size="10"
                    color="#878787"
                  />
                </StyledArrow>
              )}
            </StyledRightArrowWrapper>
          }
          arrowRightDisabled={
            <StyledRightArrowWrapper
              onClick={() => {
                if (value < children.length - slidesPerPage) {
                  onChange(value + 1);
                }
              }}
            >
              {ArrowComponent ? (
                <ArrowComponent>
                  <Icon
                    name="Arrow-Down"
                    rotate={-270}
                    size="10"
                    color="#878787"
                  />
                </ArrowComponent>
              ) : (
                <StyledArrow>
                  <Icon
                    name="Arrow-Down"
                    rotate={-270}
                    size="10"
                    color="#878787"
                  />
                </StyledArrow>
              )}
            </StyledRightArrowWrapper>
          }
          value={value}
          onChange={(item) => onChange(item)}
          slides={children}
        ></ReactCarousel>
        {renderDots()}
      </StyledCarousel>
    </>
  );
};

Carousel.defaultProps = {
  infinite: false,
};

Carousel.propTypes = {
  children: PropTypes.node,
  slidesPerPage: PropTypes.number,
  ArrowComponent: PropTypes.node,
  infinite: PropTypes.bool,
  CustomDots: PropTypes.node,
};

export default Carousel;
