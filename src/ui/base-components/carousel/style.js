import styled from 'styled-components';
const StyledCarousel = styled.div``;

const StyledArrow = styled.div`
  width: 40px;
  height: 40px;
  background-color: #1b1b1f;
  border-radius: 50%;
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const StyledDot = styled.div`
  width: ${({ active }) => (active ? '16px' : '8px')};
  height: 8px;
  border-radius: 4px;
  background-color: ${({ active }) =>
    active ? '#292929' : '#292929'};
  margin-left: 8px;
  cursor: pointer;
`;

const StyledDotWrapper = styled.div`
  margin-top: 32px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const StyledLeftArrowWrapper = styled.div`
  div {
    cursor: pointer;
    padding-left: 3px;
  }
`;

const StyledRightArrowWrapper = styled.div`
  div {
    cursor: pointer;
    padding-right: 3px;
  }
`;

export {
  StyledDotWrapper,
  StyledDot,
  StyledCarousel,
  StyledArrow,
  StyledRightArrowWrapper,
  StyledLeftArrowWrapper,
};
