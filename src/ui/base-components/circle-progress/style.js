import styled from 'styled-components';
import { createGlobalStyle } from 'styled-components';

const ReplaceCircleProgressLibStyles = createGlobalStyle`
.CircularProgressbar .CircularProgressbar-text {
  transform: translateY(6px);
}

`;

const StyledWrapper = styled.div`
  width: 24px;
`;
export default ReplaceCircleProgressLibStyles;
export { StyledWrapper };
