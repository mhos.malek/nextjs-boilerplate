import {
  CircularProgressbar,
  buildStyles,
} from 'react-circular-progressbar';
import PropTypes from 'prop-types';
import { StyledWrapper } from './style';
const circleProgressBar = ({ percentage, children }) => {
  return (
    <>
      <StyledWrapper>
        <CircularProgressbar
          value={percentage}
          text={`${children}`}
          strokeWidth={8}
          styles={buildStyles({
            textColor: '#eaeaea',
            pathColor: '#027aff',
            trailColor: '#68687a',
            textSize: '4em',
          })}
        />
      </StyledWrapper>
    </>
  );
};

circleProgressBar.propTypes = {
  percentage: PropTypes.number,
  children: PropTypes.node,
};
export default circleProgressBar;
