import PropTypes from 'prop-types';
import Styled from './style';
import Typography from '../typography';

const Checkbox = ({ label, checked, onChange }) => {
  return (
    <Styled onClick={() => onChange(!checked)}>
      <input
        type="checkbox"
        checked={checked}
        onChange={() => onChange(!checked)}
      />
      <label>
        <Typography type="regular" size="16" color="browngrey">
          {label}
        </Typography>
      </label>
    </Styled>
  );
};

Checkbox.propTypes = {
  checked: PropTypes.bool,
  onChange: PropTypes.func,
  label: PropTypes.string,
};

export default Checkbox;
