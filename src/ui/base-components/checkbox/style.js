import styled from 'styled-components';
const Styled = styled.div`
  display: flex;
  > input {
    opacity: 0;
    position: absolute;
  }
  > input + label {
    position: relative;
    padding-right: 36px;
    cursor: pointer;
    height: 23px;
    display: flex;
    align-items: center;
    transition: all 0.3s;
    &:after {
      content: '';
      position: absolute;
      right: 0;
      top: 1px;
      width: 17px;
      height: 17px;
      background: ${({ theme }) => theme.defaultColors.transparent};
      border: 2px solid ${({ theme }) => theme.defaultColors.gray};
      border-radius: 3px;
    }
    &:before {
      content: '';
      position: absolute;
      right: 6px;
      top: 4px;
      width: 6px;
      height: 8px;
      border: solid white;
      border-width: 0 3px 3px 0;
      border-color: ${({ theme }) => theme.defaultColors.dark};
      transform: rotate(45deg) !important;
      transition: all 0.3s;
      z-index: 2;
    }
  }
  > input:not(:checked) + label {
    &:before {
      opacity: 0;
      transform: scale(0);
    }
  }
  > input:disabled:not(:checked) + label {
    &:after {
      box-shadow: none;
      border-color: #bbb;
      background-color: #ddd;
    }
  }
  > input:checked + label {
    &:after {
      background: ${({ theme }) => theme.defaultColors.primary};
      border-color: ${({ theme }) => theme.colors.transparent};
      transition: all 0.3s;
    }
    &:before {
      opacity: 1;
      transform: scale(1);
    }
  }
  > input:disabled:checked + label {
    &:before {
      color: #999;
    }
  }
  > input:disabled + label {
    color: #aaa;
  }
  > input:checked:focus + label,
  input:not(:checked):focus + label {
    &:after {
      border: 1px dotted blue;
    }
  }
`;

export default Styled;
