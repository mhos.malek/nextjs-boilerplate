import styled from 'styled-components';

const StyledMargin = styled.div`
  ${({ theme, x }) =>
    //   x axis margin (right and left)
    x &&
    `margin-right: ${theme.dimensions[x]};
                        margin-left: ${theme.dimensions[x]}`};

  ${({ theme, y }) =>
    // y axis margin (bottom and top)
    y &&
    `margin-top: ${theme.dimensions[y]};
                        margin-bottom: ${theme.dimensions[y]}`};

  ${({ theme, right }) =>
    // right  margin

    right &&
    `margin-right: ${theme.dimensions[right] || right};
                        `};

  ${({ theme, left }) =>
    // left  margin
    left &&
    `margin-left: ${theme.dimensions[left] || left};
                      `};

  ${({ theme, top }) =>
    // top  margin
    top &&
    `margin-top: ${theme.dimensions[top] || top};
                  `};

  ${({ theme, bottom }) =>
    // bottom  margin
    bottom &&
    `margin-bottom: ${theme.dimensions[bottom] || bottom};
            `};
  ${({ theme, all }) =>
    // all  margins
    all &&
    `margin: ${theme.dimensions[all]};
    `};
  ${({ inline }) =>
    // all  margins
    inline &&
    `display: inline-block;
    `};
  ${({ flex }) =>
    // all  margins
    flex &&
    `display: flex;
    `};
`;

export default StyledMargin;
