import PropTypes from 'prop-types';
import StyledMargin from './style';

const Margins = ({
  children,
  x,
  y,
  right,
  left,
  top,
  bottom,
  all,
  inline,
  flex,
}) => (
  <StyledMargin
    x={x}
    y={y}
    right={right}
    left={left}
    top={top}
    bottom={bottom}
    all={all}
    inline={inline}
    flex={flex}
  >
    {children}
  </StyledMargin>
);
Margins.propTypes = {
  children: PropTypes.node,
  x: PropTypes.string,
  y: PropTypes.string,
  right: PropTypes.string,
  left: PropTypes.string,
  top: PropTypes.string,
  bottom: PropTypes.string,
  all: PropTypes.string,
  inline: PropTypes.bool,
  flex: PropTypes.bool,
};
export default Margins;
