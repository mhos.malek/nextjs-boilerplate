import PropTypes from 'prop-types';
import { StyledNavItem } from './style';

const NavItem = ({ children, onClick }) => (
  <StyledNavItem onClick={() => onClick && onClick()}>
    {children}
  </StyledNavItem>
);

NavItem.propTypes = {
  children: PropTypes.node,
  onClick: PropTypes.func,
};

export default NavItem;
