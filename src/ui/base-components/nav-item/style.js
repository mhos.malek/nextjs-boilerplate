import styled from 'styled-components';

const StyledNavItem = styled.div`
  color: ${({ theme }) => theme.colors.white};
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  @media screen and ${({ theme }) =>
      theme.mediaQueriesMaxWidth.tablet} {
    justify-content: flex-end;
  }
`;

export { StyledNavItem };
