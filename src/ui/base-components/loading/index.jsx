import PropTypes from 'prop-types';
import {
  StyledWrapper,
  StyledLoadingLineOne,
  StyledLoadingLineTwo,
  StyledLoadingLineThree,
  StyledFulldWrapper,
  StyledLoadingWapper,
  StyledLogo,
} from './style';
const Loading = ({
  color,
  fullscreen,
  coverParent,
  type,
  noBackground,
}) => {
  if (fullscreen || coverParent) {
    return (
      <StyledFulldWrapper
        fullscreen={fullscreen}
        coverParent={coverParent}
        noBackground={noBackground}
      >
        <StyledLoadingWapper noBackground={noBackground}>
          {type === 'lines' ? (
            <StyledWrapper color={color}>
              <StyledLoadingLineOne color={color} />
              <StyledLoadingLineTwo color={color} />
              <StyledLoadingLineThree color={color} />
            </StyledWrapper>
          ) : (
            <StyledLogo
              src="/assets/images/logo/pf-logo.svg"
              alt="logo"
            />
          )}
        </StyledLoadingWapper>
      </StyledFulldWrapper>
    );
  }
  return (
    <StyledWrapper color={color}>
      <StyledLoadingLineOne color={color} />
      <StyledLoadingLineTwo color={color} />
      <StyledLoadingLineThree color={color} />
    </StyledWrapper>
  );
};

Loading.propTypes = {
  color: PropTypes.string,
  fullscreen: PropTypes.bool,
  coverParent: PropTypes.bool,
  type: PropTypes.string,
  noBackground: PropTypes.bool,
};

export default Loading;
