import styled from 'styled-components';
import { keyframes } from 'styled-components';
const loadingAnimation = keyframes`
 0% { transform: scale(0.5) }
 50% { transform: scale(1) }
 100% { transform: scale(0.5) }
`;

const loadingAnimationCenterLine = keyframes`
 0% { transform: scale(1) }
 50% { transform: scale(0.5) }
 100% { transform: scale(1) }
`;

const logoAnimation = keyframes`
100% { transform: rotate(360deg) }

`;

const StyledWrapper = styled.div`
  display: flex;
`;

const StyledFulldWrapper = styled.div`
  ${({ coverParent, fullscreen }) => {
    if (fullscreen) {
      return 'position: fixed';
    }
    if (coverParent) {
      return 'position: absolute';
    }
  }};

  bottom: 0;
  top: 0;
  right: 0;
  left: 0;
  z-index: 10;
  background-color: ${({ noBackground }) =>
    noBackground ? 'transparent' : 'rgba(0, 0, 0, 0.8)'};
  display: flex;
  align-items: center;
  justify-content: center;
  opacity: 1;
  transition: all 0.3s;
`;

const StyledLoadingWapper = styled.div`
  ${({ coverParent, fullScreen }) => {
    if (coverParent) {
      return ` width: 100px;
    height: 100px;`;
    }
    if (fullScreen) {
      return ` width: 144px;
      height: 144px;`;
    }
  }};

  border-radius: 16px;
  background-color: ${({ noBackground }) =>
    noBackground ? 'transparent' : '#151518'};
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 400;
`;
const StyledLogo = styled.img`
  animation-name: ${logoAnimation};
  animation-duration: 1.3s;
  animation-iteration-count: infinite;
  animation-timing-function: linear;
`;

const StyledLoadingLineOne = styled.div`
  width: 2px;
  height: 16px;
  margin-left: 8px;
  background-color: ${({ theme, color }) =>
    theme.colors[color] || theme.defaultColors[color] || 'white'};
  animation-name: ${loadingAnimation};
  animation-duration: 0.8s;
  animation-iteration-count: infinite;
  animation-timing-function: ease-in-out;
`;
const StyledLoadingLineTwo = styled.div`
  width: 2px;
  height: 16px;
  background-color: ${({ theme, color }) =>
    theme.colors[color] || theme.defaultColors[color] || 'white'};
  margin-left: 8px;
  animation-name: ${loadingAnimationCenterLine};
  animation-iteration-count: infinite;
  animation-duration: 0.8s;
  animation-timing-function: ease-in-out;
`;
const StyledLoadingLineThree = styled.div`
  width: 2px;
  height: 16px;
  background-color: ${({ theme, color }) =>
    theme.colors[color] || theme.defaultColors[color] || 'white'};
  animation-name: ${loadingAnimation};
  animation-duration: 0.8s;
  animation-iteration-count: infinite;
  animation-timing-function: ease-in-out;
`;

export {
  StyledWrapper,
  StyledLoadingLineOne,
  StyledLoadingLineTwo,
  StyledLoadingLineThree,
  StyledFulldWrapper,
  StyledLoadingWapper,
  StyledLogo,
};
