import styled from 'styled-components';

const StyledTypography = styled.div`
  color: ${({ theme, color }) =>
    theme.colors[color] || theme.defaultColors[color] || color};
  direction: ${({ theme, direction }) =>
    direction ? theme.direction[direction] : 'rtl'};
  ${({ align }) => align && `text-align: ${align}`};
  ${({ inline }) => (inline ? 'display: inline-block' : null)};
  ${({ flex }) => (flex ? 'display: flex' : null)};
  ${({ lang, theme, size, type }) => {
    if (!type || !size) {
      return null;
    }
    return lang === 'en'
      ? theme.typography.englishFont[type][size]
      : theme.typography.defaultFont[type][size];
  }}
  ${({ truncate }) => {
    if (truncate) {
      return `width: 210px;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;`;
    }
  }}
  ${({ decoration }) =>
    decoration && `text-decoration: ${decoration}`} 

  ${({ theme, responsive, lang }) => {
    // responsive part
    if (responsive) {
      if (lang === 'en') {
        return theme.typography.englishFont[responsive.type][
          responsive.size
        ];
      } else {
        return theme.typography.defaultFont[responsive.type][
          responsive.size
        ];
      }
    }
  }}};
`;
export default StyledTypography;
