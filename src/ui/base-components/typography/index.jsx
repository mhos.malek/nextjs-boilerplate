import PropTypes from 'prop-types';
import StyledTypography from './style';
import { useRef } from 'react';

const Typography = ({
  children,
  type,
  size,
  color,
  direction,
  align,
  inline,
  flex,
  lang,
  responsive,
  dangerouslySetInnerHTML,
  truncate,
  decoration,
}) => {
  if (dangerouslySetInnerHTML) {
    return (
      <StyledTypography
        type={type}
        size={size}
        color={color}
        direction={direction}
        align={align}
        inline={inline}
        flex={flex}
        lang={lang}
        responsive={responsive}
        dangerouslySetInnerHTML={{ __html: dangerouslySetInnerHTML }}
        truncate={truncate}
        decoration={decoration}
      ></StyledTypography>
    );
  } else {
    return (
      <StyledTypography
        type={type}
        size={size}
        color={color}
        direction={direction}
        align={align}
        inline={inline}
        flex={flex}
        lang={lang}
        responsive={responsive}
        truncate={truncate}
        decoration={decoration}
      >
        {children}
      </StyledTypography>
    );
  }
};

Typography.defaultProps = {
  lang: 'fa',
};

Typography.propTypes = {
  children: PropTypes.node,
  type: PropTypes.string,
  size: PropTypes.string,
  color: PropTypes.string,
  direction: PropTypes.string,
  align: PropTypes.string,
  inline: PropTypes.bool,
  flex: PropTypes.bool,
  lang: PropTypes.string,
  responsive: PropTypes.any,
  dangerouslySetInnerHTML: PropTypes.any,
  truncate: PropTypes.bool,
};

export default Typography;
