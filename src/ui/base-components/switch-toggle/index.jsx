import PropTypes from 'prop-types';
import { StyledSwitch, StyledWrapper } from './style';
import Margins from '../margin';
import Typography from '../typography';
const SwitchToggle = ({ onChange, name, checked, label }) => {
  return (
    <StyledWrapper>
      <StyledSwitch>
        <input
          type="checkbox"
          name={name}
          checked={checked}
          onChange={(e) =>
            onChange && onChange(name, e.target.checked)
          }
        />
        <span />
      </StyledSwitch>
      <Margins x="lg" top="6px">
        <Typography type="regular" color="browngrey" size="14">
          {label}
        </Typography>
      </Margins>
    </StyledWrapper>
  );
};

SwitchToggle.propTypes = {
  onChange: PropTypes.func,
  name: PropTypes.string,
  checked: PropTypes.bool,
  label: PropTypes.string,
};
export default SwitchToggle;
