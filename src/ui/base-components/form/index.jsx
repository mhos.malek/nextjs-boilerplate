import PropTypes from 'prop-types';

const Form = ({ children, onSubmit }) => {
  const handleSubmit = (e) => {
    e.preventDefault();
    onSubmit && onSubmit();
  };
  return (
    <form
      onSubmit={(e) => {
        e.preventDefault();
        handleSubmit && handleSubmit(e);
      }}
    >
      {children}
    </form>
  );
};

Form.propTypes = {
  children: PropTypes.node,
  onSubmit: PropTypes.func,
};
export default Form;
