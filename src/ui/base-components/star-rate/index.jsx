import Rating from 'react-rating';
import PropTypes from 'prop-types';
import Icon from '../icon';
const StarRating = ({ rate, readonly, onRateChange }) => {
  return (
    <Rating
      placeholderRating={rate}
      emptySymbol={
        <Icon name="Star" size="14" color="orangeyyellow" />
      }
      placeholderSymbol={
        <Icon name="Star-filled" size="14" color="orangeyyellow" />
      }
      fullSymbol={
        <Icon name="Star-filled" size="14" color="orangeyyellow" />
      }
      readonly={readonly}
      onChange={onRateChange && onRateChange}
    />
  );
};

StarRating.propTypes = {
  rate: PropTypes.string,
  readonly: PropTypes.bool,
  onRateChange: PropTypes.func,
};

export default StarRating;
