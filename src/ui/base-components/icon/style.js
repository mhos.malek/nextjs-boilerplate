import styled, { keyframes, css } from 'styled-components';
const spinAnimation = keyframes`
100% { transform: rotate(360deg) }

`;
const StyledIcon = styled.span`
  ${({ theme, size }) => {
    return `font-size: ${theme.dimensions[size] || `${size}px`}`;
  }};
  ${({ theme, color }) => {
    return `color: ${
      theme.defaultColors[color] || theme.colors[color] || color
    }`;
  }};
  display: inline-block;
  transform: rotate(
    ${({ rotate }) => (rotate ? `${rotate}deg` : null)}
  ) !important;
  transition: all 0.3s;
  ${({ animation }) => {
    if (animation === 'spin') {
      return css`
        animation-name: ${spinAnimation};
        animation-duration: 1.3s;
        animation-iteration-count: infinite;
        animation-timing-function: linear;
      `;
    }
  }}
`;

export default StyledIcon;
