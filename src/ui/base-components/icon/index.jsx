import PropTypes from 'prop-types';
import StyledIcon from './style';
const Icon = ({ name, size, color, rotate, onClick, animation }) => (
  <StyledIcon
    className={`icon-${name}`}
    size={size}
    color={color}
    rotate={rotate}
    onClick={onClick && onClick}
    animation={animation}
  />
);

Icon.propTypes = {
  name: PropTypes.string,
  size: PropTypes.string,
  color: PropTypes.string,
  rotate: PropTypes.number,
  onClick: PropTypes.func,
  animation: PropTypes.string,
};

export default Icon;
