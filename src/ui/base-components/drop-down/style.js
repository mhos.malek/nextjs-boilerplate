import { createGlobalStyle } from 'styled-components';

const OverRideStyles = createGlobalStyle`
  .menuClassName {
    max-height: 134px;
    overflow-y: auto;
    background-color: #16161a;
    border: none;
    border-radius: 8px;
    border-top-right-radius: 0;
    border-top-left-radius: 0;
    /* width */
  ::-webkit-scrollbar {
    width: 4px;
  }

  /* Track */
  ::-webkit-scrollbar-track {
    background: ${({ theme }) => theme.colors.darktwo};
  }

  /* Handle */
  ::-webkit-scrollbar-thumb {
    background: ${({ theme }) => theme.colors.gunmetaltwo};
  }

  /* Handle on hover */
  ::-webkit-scrollbar-thumb:hover {
    background: ${({ theme }) => theme.colors.gunmetaltwo};
  }
    .Dropdown-option {
        text-align: center;
        &:hover {
            background-color: inherit;
            color: ${({ theme }) => theme.colors.verylightpink};
        }
        &.is-selected {
        background-color: inherit;
        color: ${({ theme }) => theme.colors.verylightpink};
     }
    }

  }
  .controlClassName {
    border: none;
    width: 136px;
    height: 40px;
    border-radius: 8px;
    background-color: #16161a;
    display: flex;
    flex-direction: row-reverse;
    align-items: center;
    justify-content: center;
    padding: 0;
    color: #878787;
  }
  .placeholderClassName {
    font-size: 14px;
  }
`;

export default OverRideStyles;
