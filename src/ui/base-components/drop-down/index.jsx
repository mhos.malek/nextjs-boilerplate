/* eslint-disable react/prop-types */
import ReactDropdown from 'react-dropdown';
import Icon from '../icon';
import OverRideStyles from './style';
import Margins from '../margin';
import { useState } from 'react';
const DropDown = ({ options, onSelect, placeholder }) => {
  const [value, setValue] = useState(null);
  const handleOnSelect = (value) => {
    setValue(value);
    onSelect && onSelect(value);
  };
  return (
    <>
      <OverRideStyles />
      <ReactDropdown
        arrowClosed={
          <Margins x="md">
            <Icon name="Arrow-Down" size="8" />
          </Margins>
        }
        arrowOpen={
          <Margins x="md">
            <Icon name="Arrow-up" size="8" />
          </Margins>
        }
        menuClassName="menuClassName"
        controlClassName="controlClassName"
        placeholderClassName="placeholderClassName"
        options={options}
        onChange={handleOnSelect}
        value={value}
        placeholder={placeholder}
      />
    </>
  );
};

export default DropDown;
