import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  StyledWrapper,
  StyledInputContainer,
  StyledTextArea,
  StyledIconWrapper,
} from './style';
import Typography from '../typography';
import validatorFunctions from '../../../services/validation/validator-functions';
import Margins from '../margin';
import Icon from '../icon';
import { validationTypes } from '../../../services/validation/types';

const TextArea = ({
  name,
  placeholder,
  onChange,
  disabled,
  onFocus,
  onBlur,
  customIcon,
  value,
  direction,
  validation,
  helperText,
  errorMessage,
  hasError,
  customActions,
  resize,
}) => {
  const [valueState, setValueState] = useState(value);
  const [hasValidationError, setHasValidationError] = useState(false);
  const handleOnBlur = (e) => onBlur && onBlur(e);
  const handleOnFocus = (e) => onFocus && onFocus(e);
  const handleOnChange = ({ target }) => {
    const { value } = target;
    onChange && onChange(name, value);
  };

  const handleValidation = (value) => {
    // ustom function

    if (validation.type === validationTypes.customFunction) {
      if (value.length > 0 && !validation.validateFunction()) {
        validation &&
          validation.onError &&
          validation.onError(name, true);
        return setHasValidationError(true);
      }
      value.length > 0 &&
        validation &&
        validation.onError &&
        validation.onError(name, false);
      return setHasValidationError(false);
    }

    // pre defined validation
    if (
      value.length > 0 &&
      !validatorFunctions[validation.type](
        value,
        validation.parameter
      )
    ) {
      value.length > 0 &&
        validation &&
        validation.onError &&
        validation.onError(name, true);
      return setHasValidationError(true);
    }
    value.length > 0 &&
      validation &&
      validation.onError &&
      validation.onError(name, false);
    return setHasValidationError(false);
  };

  const renderInputBottomText = () => {
    if (hasValidationError) {
      return (
        <Margins top="sm">
          <Typography type="regular" size="12" color="danger">
            {validation && validation.errorMessage}
          </Typography>
        </Margins>
      );
    }
    if (hasError) {
      return (
        <Margins top="sm">
          <Typography type="regular" size="12" color="danger">
            {errorMessage}
          </Typography>
        </Margins>
      );
    }
    if (helperText) {
      return (
        <Margins top="sm">
          <Typography
            type="regular"
            size="12"
            color="greyishbrowntwo"
          >
            {helperText}
          </Typography>
        </Margins>
      );
    }
    return null;
  };

  useEffect(() => {
    setValueState(value);
    if (validation) {
      handleValidation(value);
    }
  }, [value]);

  return (
    <StyledWrapper>
      <StyledInputContainer>
        <StyledIconWrapper>
          <Icon name={customIcon} size="23" color="browngrey" />
        </StyledIconWrapper>
        <StyledTextArea
          direction={direction}
          name={name}
          placeholder={placeholder}
          disabled={disabled}
          value={valueState}
          onBlur={handleOnBlur}
          onFocus={handleOnFocus}
          onChange={handleOnChange}
          hasError={hasValidationError || hasError}
          resize={resize}
        />
        {customActions && customActions}
      </StyledInputContainer>
      {renderInputBottomText()}
    </StyledWrapper>
  );
};
TextArea.defaultProps = {
  disabled: false,
  direction: 'rtl',
  resize: false,
};
TextArea.propTypes = {
  name: PropTypes.string,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  customIcon: PropTypes.node,
  disabled: PropTypes.bool,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  onChange: PropTypes.func,
  value: PropTypes.string,
  direction: PropTypes.string,
  validation: PropTypes.object,
  helperText: PropTypes.bool,
  hasError: PropTypes.bool,
  errorMessage: PropTypes.string,
  customActions: PropTypes.node,
  resize: PropTypes.bool,
};
export default TextArea;
