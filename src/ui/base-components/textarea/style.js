import styled from 'styled-components';

const StyledWrapper = styled.div``;

const StyledInputContainer = styled.div`
  height: 86px;
  position: relative;
`;
const StyledTextArea = styled.textarea`
  background-color: ${({ theme }) => theme.colors.darkfive};
  border: 1px solid transparent;
  width: 100%;
  height: 100%;
  outline: none;
  font-family: Dana;
  padding-left: 16px;
  padding-right: 50px;
  padding-top: 16px;
  border-radius: ${({ theme }) => theme.dimensions.sm};
  ${({ resize }) => {
    if (!resize) {
      return 'resize: none';
    }
  }};

  color: ${({ theme }) => theme.colors.verylightpink};
  direction: ${({ direction }) => direction};
  transition: all 0.3s;
  ${({ theme }) => theme.typography.defaultFont.regular[13]};
  ${({ hasError, theme }) =>
    hasError ? `border-color: ${theme.defaultColors.danger};` : null}
  &:placeholder {
    color: ${({ theme }) => theme.colors.browngrey};
  }
  &:disabled {
    &::placeholder {
      color: #565656;
    }
  }
  &:focus {
    border-color: ${({ theme }) => theme.defaultColors.primary};
    transition: all 0.3s;
  }
`;

const StyledIconWrapper = styled.div`
  position: absolute;
  right: ${({ theme }) => theme.dimensions.lg};
  top: ${({ theme }) => theme.dimensions.lg};
  height: 100%;
  display: flex;
  color: ${({ theme }) => theme.colors.browngrey};
}
`;

export {
  StyledWrapper,
  StyledInputContainer,
  StyledTextArea,
  StyledIconWrapper,
};
