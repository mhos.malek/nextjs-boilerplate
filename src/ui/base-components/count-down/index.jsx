import { useState, memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import Typography from '../typography';

const CountDown = memo(function CountDown({ value, onCompleted }) {
  const time = 60 * value;
  const [minutesDisplay, setMinutesDisplay] = useState('00');
  const [secondsDisplay, setSecondsDisplay] = useState('00');
  useEffect(() => {
    const startTimer = (duration) => {
      let timer = duration;
      let minutes;
      let seconds;

      setInterval(() => {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? '0' + minutes : minutes;
        seconds = seconds < 10 ? '0' + seconds : seconds;
        setMinutesDisplay(minutes);
        setSecondsDisplay(seconds);

        if (--timer < 0) {
          timer = duration;
          onCompleted && onCompleted();
        }
      }, 1000);
    };

    startTimer(time);
  }, []);

  return (
    <Typography inline color="white" size="14" type="normal">
      {minutesDisplay}:{secondsDisplay}
    </Typography>
  );
});

CountDown.propTypes = {
  value: PropTypes.number,
  onCompleted: PropTypes.func,
};

export default CountDown;
