import styled from 'styled-components';

const StyledActiveAccordion = styled.div`
  padding: 8px 0;
`;

export { StyledActiveAccordion };
