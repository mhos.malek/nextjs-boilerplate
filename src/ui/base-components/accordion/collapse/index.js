import React from 'react';
import PropTypes from 'prop-types';
import { StyledActiveAccordion } from './style';
import { useAccordionContext } from '../hooks';
import Seperator from '../../seperator';
const Collapse = ({
  element: Component,
  eventKey,
  children,
  ...otherProps
}) => {
  const { activeEventKey } = useAccordionContext();

  return activeEventKey === eventKey ? (
    <>
      <StyledActiveAccordion>
        <Component {...otherProps}>{children}</Component>
      </StyledActiveAccordion>
      <Seperator />
    </>
  ) : (
    <Seperator />
  );
};

Collapse.propTypes = {
  element: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
  eventKey: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  children: PropTypes.node,
};

Collapse.defaultProps = {
  element: 'div',
};

export default Collapse;
