import React from 'react';
import PropTypes from 'prop-types';
import { useAccordionContext } from '../hooks';
import Icon from '../../icon';
import { StyledToggle } from './style';
const useAccordionClick = (eventKey, onClick) => {
  const { onToggle, activeEventKey } = useAccordionContext();
  return (event) => {
    onToggle(eventKey === activeEventKey ? null : eventKey);

    if (onClick) {
      onClick(event);
    }
  };
};

const Toggle = ({
  element: Component,
  eventKey,
  onClick,
  children,
  ...otherProps
}) => {
  const accordionClick = useAccordionClick(eventKey, onClick);
  const { activeEventKey } = useAccordionContext();
  const isActive = () => activeEventKey === eventKey;
  return (
    <Component onClick={accordionClick} {...otherProps}>
      <StyledToggle isActive={isActive()}>
        {children}
        <Icon
          name="Arrow-up"
          color="bluegrey"
          size="5"
          rotate={isActive() ? 0 : 180}
        />
      </StyledToggle>
    </Component>
  );
};

Toggle.propTypes = {
  element: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
  eventKey: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  children: PropTypes.node,
  onClick: PropTypes.func,
};

Toggle.defaultProps = {
  element: 'div',
};

export default Toggle;
