import styled from 'styled-components';

const StyledToggle = styled.div`
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: space-between;
  border-radius: 8px;
  padding: 0 24px;
  background-color: ${({ isActive, theme }) =>
    isActive && theme.colors.darktwo};
  transition: all 0.3s;
`;

export { StyledToggle };
