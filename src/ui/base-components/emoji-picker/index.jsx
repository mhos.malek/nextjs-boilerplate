import { Picker } from 'emoji-mart';
import PropTypes from 'prop-types';
import OverRideStyles from './style';
import { useEffect, useState, useRef } from 'react';
const EmojiPicker = ({ onSelect, show }) => {
  const [showEmojiPicker, setShowEmojiPicker] = useState(false);
  const handleEmojiSelect = (emoji) => {
    onSelect && onSelect(emoji);
  };
  const emojisList = [
    '1F604',
    '1F602',
    '1F605',
    '1F61D',
    '1F613',
    '1F603',
    '1F60A',
    '1F4A9',
  ];
  const emojiPickerElem = useRef(null);

  const outsideClickHandler = (event) => {
    const isClickInside = emojiPickerElem.current.contains(
      event.target
    );


    if (isClickInside) {
      return null;
    }
    setShowEmojiPicker(false);
    return;
  };

  useEffect(() => {
    setShowEmojiPicker(show);
  }, [show]);

  useEffect(() => {
    setShowEmojiPicker(false);
    window.addEventListener('click', outsideClickHandler);
    return function () {
      window.removeEventListener('click', outsideClickHandler);
    };
  }, []);
  return (
    <div dir="ltr" ref={emojiPickerElem}>
      <OverRideStyles />
      {showEmojiPicker && (
        <Picker
          onSelect={handleEmojiSelect}
          emoji=""
          emojisToShowFilter={(emoji) =>
            emojisList.includes(emoji.unified)
          }
          perLine={6}
          theme="dark"
          showPreview={false}
          showSkinTones={false}
          emojiTooltip={false}
          title=""
        />
      )}
    </div>
  );
};

EmojiPicker.propTypes = {
  onSelect: PropTypes.func,
  show: PropTypes.bool,
};
export default EmojiPicker;
