import { createGlobalStyle } from 'styled-components';

const OverrideStyles = createGlobalStyle`
.emoji-mart-category[aria-label="Frequently Used"] {
    display: none;
}
.emoji-mart-search {
    display: none;
}

.emoji-mart-category-label {
    display: none;
}
.emoji-mart-dark .emoji-mart-bar {
    display: none;
}
.emoji-mart-scroll {
    overflow-y: hidden;
    height: auto;
}
.emoji-mart-dark {
    width: 245px;
    /* width: 196px; */
    height: 209px;
    border-radius: 1px;
    box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.3);
    background-color: #1e1e24;
    border: none;
    padding: 16px;

}

`;

export default OverrideStyles;
