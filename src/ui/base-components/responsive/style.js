import styled from 'styled-components';

const StyledResponsiveUtil = styled.div`
  display: none;
  @media screen and ${({ showAtMediaQuery }) => showAtMediaQuery} {
    display: block;
  }
`;

export default StyledResponsiveUtil;
