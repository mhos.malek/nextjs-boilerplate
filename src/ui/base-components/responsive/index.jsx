import PropTypes from 'prop-types';
import StyledResponsiveUtil from './style';
const Responsive = ({ children, showAtMediaQuery }) => {
  return (
    <StyledResponsiveUtil showAtMediaQuery={showAtMediaQuery}>
      {children}
    </StyledResponsiveUtil>
  );
};

Responsive.propTypes = {
  children: PropTypes.node,
  show_at_device: PropTypes.string,
  showAtMediaQuery: PropTypes.string,
};

export default Responsive;
