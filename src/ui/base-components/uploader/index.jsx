/* eslint-disable react/prop-types */
import Dropzone from 'react-dropzone-uploader';
import { getDroppedOrSelectedFiles } from 'html5-file-selector';
import {
  StyledLabel,
  StyledButton,
  StyledInputWrapper,
  StyledRemoveIcon,
} from './style';
import Icon from '../icon';
import Typography from '../typography';
import Margins from '../margin';

const CustomInputForUploader = ({
  accept,
  onFiles,
  files,
  getFilesFromEvent,
}) => {
  const text =
    files.length > 0
      ? files[0].file && files[0].file.name
      : 'فایل خود را انتخاب کنید';

  return (
    <>
      <label>
        <StyledInputWrapper>
          <StyledLabel>
            {text}
            <input
              style={{ display: 'none' }}
              type="file"
              accept={accept}
              onChange={(e) => {
                getFilesFromEvent(e).then((chosenFiles) => {
                  onFiles(chosenFiles);
                });
              }}
            />
            {files.length > 0 && (
              <StyledRemoveIcon
                onClick={(e) => {
                  e.preventDefault();
                  files.forEach((f) => f.remove());
                }}
              >
                <Icon name="Play-2" />
              </StyledRemoveIcon>
            )}
          </StyledLabel>
          <StyledButton>انتخاب فایل</StyledButton>
        </StyledInputWrapper>
      </label>
    </>
  );
};

const Uploader = ({ label, supportedFormats }) => {
  const options = {
    multiple: false,
  };
  const handleSubmit = (files, allFiles) => {
    console.log(files.map((f) => f.meta));
    console.log(allFiles);
  };

  const getFilesFromEvent = (e) => {
    return new Promise((resolve) => {
      getDroppedOrSelectedFiles(e).then((chosenFiles) => {
        resolve(chosenFiles.map((f) => f.fileObject));
      });
    });
  };
  return (
    <>
      <Margins y="md">
        <Typography size="14" type="regular" color="browngrey">
          {label}
        </Typography>
      </Margins>

      <Dropzone
        accept={supportedFormats || 'image/*,.pdf'}
        onSubmit={handleSubmit}
        InputComponent={CustomInputForUploader}
        getFilesFromEvent={getFilesFromEvent}
        PreviewComponent={null}
        SubmitButtonComponent={null}
        {...options}
      />
    </>
  );
};

export default Uploader;
