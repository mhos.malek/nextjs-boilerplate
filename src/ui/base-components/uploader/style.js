import styled from 'styled-components';

const StyledInputWrapper = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  font-size: 14px;
`;

const StyledLabel = styled.div`
  background-color: ${({ theme }) => theme.colors.darkfive};
  color: ${({ theme }) => theme.colors.browngrey};
  height: 40px;
  border-radius: 4px;
  padding: 0 16px;
  width: 100%;
  display: flex;
  align-items: center;
`;

const StyledButton = styled.div`
  background-color: ${({ theme }) => theme.colors.darkfive};
  color: white;
  height: 40px;
  min-width: 150px;
  border-radius: 4px;
  margin-right: 16px;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 0 16px;
`;

const StyledRemoveIcon = styled.div`
  padding: 20px 20px 20px 0;
  margin-top: 7px;
  z-index: 100;
  margin-right: auto;
  cursor: pointer;
`;

export {
  StyledLabel,
  StyledButton,
  StyledInputWrapper,
  StyledRemoveIcon,
};
