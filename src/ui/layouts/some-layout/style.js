import styled, { createGlobalStyle } from 'styled-components';

const GeneralStyleAuthLayout = createGlobalStyle`
body {
  background-color: #0e0e10;
}

`;

const StyledPageContainer = styled.div`
  display: flex;
  flex-direction: column;
  @media screen and ${({ theme }) =>
      theme.mediaQueriesMaxWidth.tablet} {
    height: auto;
    min-height: auto;
  }
`;
const StyledLeftSide = styled.div`
  position: relative;
  top: 0;
  bottom: 0;
  width: 55vw;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  text-align: right;
  @media screen and ${({ theme }) =>
      theme.mediaQueriesMaxWidth.tablet} {
    width: 100%;
    background-image: url('/assets/images/backgrounds/pf-logo-bg.svg');
    background-size: contain;
    background-position: center;
    background-position-y: 93px;
    background-repeat: no-repeat;
  }
`;
const StyledRightSide = styled.div`
  position: relative;
  right: 0;
  top: 0;
  bottom: 0;
  width: 45vw;
  min-height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #09090b;
`;

const StyledLogo = styled.img``;

const StyledBackgroundWrapper = styled.div`
  padding-top: 80px;
  padding-bottom: 80px;
`;
const StyledBackground = styled.img`
  max-width: 100%;
  max-height: 80vh;
`;

const StyledLogoWrapper = styled.div`
  margin-top: 100px;
`;
const StyledFooterText = styled.div`
  margin: 0 auto;
  margin-top: auto;
`;

const StyledContentWarpper = styled.div`
  display: flex;
`;

export {
  StyledPageContainer,
  StyledLeftSide,
  StyledRightSide,
  StyledBackgroundWrapper,
  StyledBackground,
  StyledLogo,
  StyledLogoWrapper,
  StyledFooterText,
  StyledContentWarpper,
};
export default GeneralStyleAuthLayout;
