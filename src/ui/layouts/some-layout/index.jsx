import PropTypes from "prop-types";
import Container from "../../base-components/container";
import GeneralStyleAuthLayout from "./style";

const AuthLayout = ({ children }) => {
  return (
    <>
      <GeneralStyleAuthLayout />
      <header>nav</header>
    </>
  );
};

AuthLayout.propTypes = {
  children: PropTypes.node,
};

export default AuthLayout;
