import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";

const actionTypes = {
  SOME_ACTION_TYPE: "SOME_ACTION_TYPE",
};

const initialState = {
  someValue: "value",
};

export const someAction = (payload) => ({
  type: actionTypes.SOME_ACTION_TYPE,
  payload,
});

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SOME_ACTION_TYPE:
      return {
        ...state,
        login: {
          ...state.login,
          someValue: action.payload,
        },
      };

    default:
      return state;
  }
};

export const initializeStore = (preloadedState = initialState) => {
  return createStore(
    reducer,
    preloadedState,
    composeWithDevTools(applyMiddleware())
  );
};
