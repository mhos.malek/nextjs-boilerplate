import fetch from "isomorphic-unfetch";
class HttpClient {
  constructor() {
    this.baseApiUrl = process.env.BASE_API_URL;
    this.apiVersion = process.env.API_VERSION;
    this.baseUrl = `${this.baseApiUrl}/${this.apiVersion}`;
    this.defaultHeaders = {
      "Content-Type": "application/json",
    };
    if (process.browser) {
      this.headers = this.defaultHeaders;
    }
  }
  createHaders = ({ token }) => {
    if (token) {
      this.headers = {
        ...this.headers,
        Authorization: `Bearer ${token}`,
      };
    } else {
      this.headers = this.defaultHeaders;
    }
  };
  handleData = async (response) => {
    const data = await response.json();
    if (response.ok) {
      return Promise.resolve(data);
    } else {
      return Promise.reject(data);
    }
  };
  get = async (url, params, token) => {
    this.createHaders({ token });
    try {
      const response = await fetch(`${this.baseUrl}/${url}`, {
        method: "GET",
        headers: this.headers,
        body: JSON.stringify(params),
      });
      const data = await response.json();
      this.handleData(data);
    } catch (error) {
      return Promise.reject(error);
    }
  };

  put = async (url, params, token) => {
    this.createHaders({ token });
    try {
      const response = await fetch(`${this.baseUrl}/${url}`, {
        method: "PUT",
        headers: this.headers,
        body: JSON.stringify(params),
      });
      const data = await response.json();
      this.handleData(data);
    } catch (error) {
      return Promise.reject(error);
    }
  };

  patch = async (url, params, token) => {
    this.createHaders({ token });
    try {
      const response = await fetch(`${this.baseUrl}/${url}`, {
        method: "patch",
        headers: this.headers,
        body: JSON.stringify(params),
      });
      const data = await response.json();
      this.handleData(data);
    } catch (error) {
      return Promise.reject(error);
    }
  };

  delete = async (url, params, token) => {
    this.createHaders({ token });
    try {
      const response = await fetch(`${this.baseUrl}/${url}`, {
        method: "DELETE",
        headers: this.headers,
        body: JSON.stringify(params),
      });
      const data = await response.json();
      this.handleData(data);
    } catch (error) {
      return Promise.reject(error);
    }
  };
}

export default new HttpClient();
