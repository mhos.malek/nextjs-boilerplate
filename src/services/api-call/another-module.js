import httpClient from "../http-client";

export const someApiCall = (parameter) => {
  return httpClient.post("/some-end-point", {
    parameter,
  });
};
