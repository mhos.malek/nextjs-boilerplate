const errorMessages = {
  ERROR_TYPE: {
    ERROR: "something went wrong",
    ERROR_SECOND_IN_THIS_TYPE: "something went wrong",
  },
  DEFAULT_MESSAGE: "this is default error message",
};

export default errorMessages;
