import { Component } from 'react';
import { checkAuth, getToken } from './token-util';
import Router from 'next/router';
const getDisplayName = (Component) =>
  Component.displayName || Component.name || 'Component';

export const withAuth = (WrappedComponent) =>
  class extends Component {
    static displayName = `withAuth(${getDisplayName(
      WrappedComponent
    )})`;

    static async getServerSideProps(ctx) {
      if (!checkAuth(ctx)) {
        if (process.browser) {
          Router.push('/auth/login');
          return;
        }
        ctx.res.writeHead(302, { Location: '/auth/login' });
        ctx.res.end();
        return;
      }
      const { refresh_token, access_token } = getToken(ctx);
      const componentProps =
        WrappedComponent.getServerSideProps &&
        (await WrappedComponent.getServerSideProps(ctx));

      return { ...componentProps, refresh_token, access_token };
    }

    // New: We bind our methods
    constructor(props) {
      super(props);
      this.syncLogout = this.syncLogout.bind(this);
    }

    // New: Add event listener when a restricted Page Component mounts
    componentDidMount() {
      window.addEventListener('storage', this.syncLogout);
    }

    // New: Remove event listener when the Component unmount and
    // delete all data
    componentWillUnmount() {
      window.removeEventListener('storage', this.syncLogout);
    }

    // New: Method to redirect the user when the event is called
    syncLogout(event) {
      if (event.key === 'logout') {
        Router.push('/auth/login');
      }
    }

    render() {
      return <WrappedComponent {...this.props} />;
    }
  };
