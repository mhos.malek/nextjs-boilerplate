import nextCookie from 'next-cookies';
import cookies from 'js-cookie';
import Router from 'next/router';

export const checkAuth = (ctx) => {
  const { access_token } = nextCookie(ctx);
  if (ctx.req && !access_token) {
    return false;
  }

  if (!access_token) {
    return false;
  }

  return true;
};

export const getToken = (ctx) => {
  const { access_token, refresh_token } = nextCookie(ctx);
  return {
    access_token,
    refresh_token,
  };
};

export const setToken = ({ access_token, refresh_token }) => {
  cookies.set('access_token', access_token);
  cookies.set('refresh_token', refresh_token);
};

export const LoginSuccess = () => {
  Router.push('/');
};

export const logout = () => {
  if (process.browser) {
    window.localStorage.setItem('logout', Date.now());
  }
  cookies.remove('access_token');
  cookies.remove('refresh_token');
};
