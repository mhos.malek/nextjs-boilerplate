import { useEffect } from 'react';
import { checkAuth } from './token-util';
import Router from 'next/router';
// const getDisplayName = (Component) =>
//   Component.displayName || Component.name || 'Component';

export const isAuthPage = (WrappedComponent) => {
  const Component = (props) => {
    // New: Method to redirect the user when the event is called
    const syncLogout = (event) => {
      if (event.key === 'logout') {
        Router.push('/auth/login');
      }
    };
    useEffect(() => {
      window.addEventListener('storage', syncLogout);
      return function () {
        // New: Remove event listener when the Component unmount and

        window.removeEventListener('storage', syncLogout);
      };
    }, []);

    // New: Add event listener when a restricted Page Component mounts

    return <WrappedComponent {...props} />;
  };
  Component.getInitialProps = async (ctx) => {
    const componentProps =
      WrappedComponent.getInitialProps &&
      (await WrappedComponent.getInitialProps(ctx));

    if (checkAuth(ctx)) {
      if (process.browser) {
        Router.push('/');
        return;
      }
      ctx.res.writeHead(302, { Location: '/' });
      ctx.res.end();
      return;
    }

    return { ...componentProps };
  };

  return Component;
};
