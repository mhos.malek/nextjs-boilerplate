const onlyNumberValidationPattern = /^[0-9]+$/;
const emailPattern = /^\S+@\S+\.\S+$/;
const userPattern = /^[a-z][a-z0-9_.]{2,64}$/;
const phoneNumberPattern = /^9[0-9]{9}$/;
const iranianPHoneNumber = /^0[0-9]{10}$/;
const passwordPattern = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d!@#$%^&*/]{8,64}$/;

export {
  onlyNumberValidationPattern,
  emailPattern,
  phoneNumberPattern,
  userPattern,
  iranianPHoneNumber,
  passwordPattern,
};
