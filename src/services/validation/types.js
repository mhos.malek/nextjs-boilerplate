export const ValidationRule = {
  parameter: 0,
  errorMessage: 'error',
};

export const validationTypes = {
  maxLength: 'maxLength',
  minLength: 'minLength',
  onlyNumber: 'onlyNumber',
  isRequired: 'isRequired',
  email: 'email',
  customPattern: 'customPattern',
  phoneNumber: 'phoneNumber',
  iranianPhoneNumber: 'iranianPhoneNumber',
  userName: 'userName',
  password: 'password',
  customFunction: 'customFunction',
};
