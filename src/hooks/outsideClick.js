import { useEffect } from "react";

const useOutsideClick = (elementRef, outsideCallBack) => {
  useEffect(() => {
    const listener = (event) => {
      // Do nothing if clicking ref's element or descendent elements
      if (!elementRef.current || elementRef.current.contains(event.target)) {
        return;
      }

      outsideCallBack(event);
    };

    document.addEventListener("mousedown", listener);
    document.addEventListener("touchstart", listener);

    return () => {
      document.removeEventListener("mousedown", listener);
      document.removeEventListener("touchstart", listener);
    };
  }, [elementRef, outsideCallBack]);
};

export default useOutsideClick;
