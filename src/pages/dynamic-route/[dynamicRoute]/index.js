/* eslint-disable react/prop-types */
import Head from "next/head";
import CoursePage from "../../../ui/pages/course";
import nextCookie from "next-cookies";
import { httpClient } from "../../../services/http-client/private-client";
import Router from "next/router";

const CourseMainPage = ({ props }) => (
  <>
    <Head>
      <title>Custom header with pages</title>
    </Head>
    <CoursePage serverProps={props} />
  </>
);

CourseMainPage.getInitialProps = async (context) => {
  // fetch data and handle authentication
  // you can use your httpClient in here to.
  // you can handle authentication with next/Router
  return {
    props: {
      courseData: "this will be passed as props to component",
    },
  };
};

export default CourseMainPage;
