import React from 'react';
import App from 'next/app';
import RootProvider from '../providers';
import 'video.js/dist/video-js.css';
import 'react-circular-progressbar/dist/styles.css';
import '@brainhubeu/react-carousel/lib/style.css';
import 'emoji-mart/css/emoji-mart.css';
import 'glider-js/glider.min.css';
import 'react-dropdown/style.css';
import '../providers/theme-provider/styles/fonts-style.css';
import 'react-toastify/dist/ReactToastify.css';


import { Provider } from 'react-redux';
import withRedux from 'next-redux-wrapper';
import { initializeStore } from '../store';

class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    // We can dispatch from here too
    const pageProps = Component.getInitialProps
      ? await Component.getInitialProps(ctx)
      : {};
    const pageServerSideProps = Component.getServerSideProps
      ? await Component.getServerSideProps(ctx)
      : {};

    return { pageProps, pageServerSideProps };
  }

  render() {
    const {
      Component,
      pageProps,
      pageServerSideProps,
      store,
    } = this.props;
    return (
      <RootProvider>
        <Provider store={store}>
          <Component {...pageProps} {...pageServerSideProps} />
        </Provider>
      </RootProvider>
    );
  }
}

export default withRedux(initializeStore)(MyApp);
