import { withAuth } from "../../services/auth/withAuthHoc";

const pageSampleRoute = () => <p>sample private page with route </p>;
// with auth will redirect user to login page if it hasn't access token in their cookie
export default withAuth(pageSampleRoute);
