import Head from 'next/head';
import HomePage from '../ui/pages/home';

const Home = ({ serverProps }) => (
  <>
    <Head>
      <title>Product Factory | Video Course</title>
    </Head>
    <HomePage serverProps={serverProps} />
  </>
);

Home.getInitialProps = async () => {
  let res;
  try {
    res = await getSomeData();
  } catch (e) {
    return e;
  }
  return { serverProps: res }; // You can pass some custom props to the component from here
};

export default Home;
