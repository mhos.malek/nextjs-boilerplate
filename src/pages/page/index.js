import { withAuth } from "../../services/auth/withAuthHoc";

const pageSampleRoute = () => <p>sample public page with route /page</p>;
// pages that has no auth check
export default pageSampleRoute;
